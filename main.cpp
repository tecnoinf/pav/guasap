#include "App/Menu.h"

int main() {

    // Iniciamos el menu principal del programa
    Menu* menu = Menu::getInstance();
    menu->mainMenu();

    return 0;
}
