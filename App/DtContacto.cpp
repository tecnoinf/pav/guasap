
#include "DtContacto.h"

#include <string>
#include <iostream>

using std::string;

//Constructores
DtContacto::DtContacto(){

    this->numTelefono = "";
    this->nombre = "";
}

DtContacto::DtContacto(string numTelefono, string nombre){

    this->numTelefono = numTelefono;
    this->nombre = nombre;
}

//Getters

string DtContacto::getNumTelefono(){

    return numTelefono;
}

string DtContacto::getNombre(){

    return nombre;
}


std::ostream& operator <<(std::ostream& out, DtContacto& dtC) {
    out << "Número de Teléfono: " << dtC.getNumTelefono() << ", Nombre: " << dtC.getNombre() << std::endl;
    return out;
}