
#include "ConversacionController.h"
#include "../Privada.h"
#include "../Simple.h"
#include "../Video.h"
#include "../Reloj.h"
#include "../Grupal.h"


string generarCodigos(int CodConversacion){

    string codMens, codConvString, codConv;
    int codMensaje = rand() % 1000 + 1;
    codMens = to_string(codMensaje);
    codConv = to_string(CodConversacion);
    codConvString = codConv + "-" + codMens;
    return codConvString;
}

int generarCodigoConv(){

        int codConv = rand() % 1000 + 1;
        return codConv;
}

ConversacionController* ConversacionController::instance = nullptr;

ConversacionController* ConversacionController::getInstance(){

    if(instance==nullptr){
        instance = new ConversacionController;
    }
    return instance;
}

void ConversacionController::archivarConversacion(int id){

    Usuario* us = UsuarioController::getInstance()->getUsuario();
    if(us!= nullptr){
        us->getColUsCon(id)->setEstaActiva(false);
    }
}

void ConversacionController::enviarMensajeContacto(string celular) {

    string codConvString;
    int codConv;
    srand((unsigned int) time(nullptr));

    // Obtenemos el usuario con sesion activa
    Usuario* us = UsuarioController::getInstance()->getUsuario();

    if (this->memConv != nullptr) {
        // Se genera un codigo aleatorio
        do {
            codConvString = generarCodigos(this->memConv->getId());
        } while(colMensajes[codConvString] != nullptr);

        Contacto* conta = new Contacto(codConvString, Reloj::getInstance()->dimeLaHora());

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = conta;

        // Se obtiene la referencia al usuario que se enviara en el mensaje tipo contacto,
        // y se lo asigna al mensaje.
        Usuario* usuAEnviar = us->contactoAEnviar(celular);
        conta->agregarContactoAMsj(usuAEnviar);

        // Enviamos el mensaje y hacemos que la conversacion sea activa.
        this->memConv->enviarMensaje(conta);
        this->memConv->setActiva(true);

    } else {
        // Se genera un codigo aleatorio para la conversacion.
        do {
            codConv = generarCodigoConv();
        } while(colConversaciones[codConv] != nullptr);

        auto privada = new Privada(codConv, memCont);

        // Agregamos la convesacion a la coleccion del controller.
        this->colConversaciones[codConv] = privada;

        // Se genera un codigo aleatorio para el mensaje.
        do {
            codConvString = generarCodigos(codConv);
        } while(colMensajes[codConvString] != nullptr);

        Contacto* conta = new Contacto(codConvString, Reloj::getInstance()->dimeLaHora());

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = conta;

        auto uem = us->iniciarConversacion(privada);            // UsuarioConversacion del emisor.
        auto ure = this->memCont->iniciarConversacion(privada); // UsuarioConversacion del receptor.

        // Se obtiene la referencia al usuario que se enviara en el mensaje tipo contacto,
        // y se lo asigna al mensaje.
        Usuario* aEnviar = us->contactoAEnviar(celular);
        conta->agregarContactoAMsj(aEnviar);

        // Enviamos el mensaje
        privada->enviarMensaje(conta, uem, ure);
    }
}

void ConversacionController::enviarMensajeImagen(string url, FormatoImg formato, int tam, string desc) {

    string codConvString;
    int codConv;
    srand((unsigned int) time(nullptr));

    Usuario* us = UsuarioController::getInstance()->getUsuario();

    if (this->memConv != nullptr) {
        // Se genera un codigo aleatorio.
        do {
            codConvString = generarCodigos(this->memConv->getId());
        } while(colMensajes[codConvString] != nullptr);

        Imagen* imag = new Imagen(codConvString, Reloj::getInstance()->dimeLaHora(), formato, tam, desc, url);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = imag;

        // Enviamos el mensaje y hacemos que la conversacion sea activa.
        this->memConv->enviarMensaje(imag);
        this->memConv->setActiva(true);

    } else {
        // Se genera un codigo aleatorio para la conversacion.
        do {
            codConv = generarCodigoConv();
        } while(colConversaciones[codConv] != nullptr);

        auto privada = new Privada(codConv, memCont);

        // Agregamos la convesacion a la coleccion del controller.
        this->colConversaciones[codConv] = privada;

        // Se genera un codigo aleatorio para el mensaje.
        do {
            codConvString = generarCodigos(codConv);
        } while(colMensajes[codConvString] != nullptr);

        Imagen* imag = new Imagen(codConvString, Reloj::getInstance()->dimeLaHora() ,formato, tam, desc, url);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = imag;

        auto uem = us->iniciarConversacion(privada);            // UsuarioConversacion del emisor.
        auto ure = this->memCont->iniciarConversacion(privada); // UsuarioConversacion del receptor.

        // Enviamos el mensaje.
        privada->enviarMensaje(imag, uem, ure);
    }
}

void ConversacionController::enviarMensajeSimple(string text){

    string codConvString;
    int codConv;
    srand((unsigned int) (time(nullptr)));

    // Obtenemos el usuario con sesion activa
    Usuario* us = UsuarioController::getInstance()->getUsuario();

    if (this->memConv != nullptr) {
        // Se genera un codigo aleatorio.
        do {
            codConvString = generarCodigos(this->memConv->getId());
        } while(colMensajes[codConvString] != nullptr);

        Simple* simple = new Simple(codConvString, Reloj::getInstance()->dimeLaHora(), text);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = simple;

        // Enviamos el mensaje y hacemos que la conversacion sea activa.
        this->memConv->enviarMensaje(simple);
        this->memConv->setActiva(true);

    } else {
        // Se genera un codigo aleatorio para la conversacion.
        do {
            codConv = generarCodigoConv();
        } while(colConversaciones[codConv] != nullptr);

        auto privada = new Privada(codConv, memCont);

        // Agregamos la convesacion a la coleccion del controller.
        this->colConversaciones[codConv] = privada;

        // Se genera un codigo aleatorio para el mensaje.
        do {
            codConvString = generarCodigos(codConv);
        } while(colMensajes[codConvString] != nullptr);

        Simple* simple = new Simple(codConvString, Reloj::getInstance()->dimeLaHora(), text);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = simple;

        auto uem = us->iniciarConversacion(privada);            // UsuarioConversacion del emisor.
        auto ure = this->memCont->iniciarConversacion(privada); // UsuarioConversacion del receptor.

        // Enviamos el mensaje.
        privada->enviarMensaje(simple, uem, ure);
    }
}

void ConversacionController::enviarMensajeVideo(string url, int duracion){

    string codConvString;
    int codConv;
    srand((unsigned int) time(nullptr));

    // Obtenemos el usuario con sesion activa
    Usuario* us = UsuarioController::getInstance()->getUsuario();

    if (this->memConv != nullptr) {
        // Se genera un codigo aleatorio.
        do {
            codConvString = generarCodigos(this->memConv->getId());
        } while(colMensajes[codConvString] != nullptr);

        Video* video = new Video(codConvString, Reloj::getInstance()->dimeLaHora(),duracion, url);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = video;

        // Enviamos el mensaje y hacemos que la conversacion sea activa.
        this->memConv->enviarMensaje(video);
        this->memConv->setActiva(true);

    } else {
        // Se genera un codigo aleatorio para la conversacion.
        do {
            codConv = generarCodigoConv();
        } while(colConversaciones[codConv] != nullptr);

        auto privada = new Privada(codConv, memCont);

        // Agregamos la convesacion a la coleccion del controller.
        this->colConversaciones[codConv] = privada;

        // Se genera un codigo aleatorio para el mensaje.
        do {
            codConvString = generarCodigos(codConv);
        } while(colMensajes[codConvString] != nullptr);

        Video* video = new Video(codConvString, Reloj::getInstance()->dimeLaHora(),duracion, url);

        // Agregamos el mensaje a la coleccion del controller.
        this->colMensajes[codConvString] = video;

        auto uem = us->iniciarConversacion(privada);            // UsuarioConversacion del emisor.
        auto ure = this->memCont->iniciarConversacion(privada); // UsuarioConversacion del receptor.

        // Enviamos el mensaje.
        privada->enviarMensaje(video, uem, ure);
    }
}

list<DtContacto *> ConversacionController::listarContactos(){

    list<DtContacto*> ret = UsuarioController::getInstance()->getUsuario()->listarContactos();
    return ret;
}

list<DtConversacion *> ConversacionController::listarConversacionesActivas(){

    list<DtConversacion *> ret = UsuarioController::getInstance()->getUsuario()->listarConversacionesActivas();
    return ret;

}

list<DtConversacion *> ConversacionController::listarConversacionesArchivadas(){

    list<DtConversacion *> ret = UsuarioController::getInstance()->getUsuario()->listarConversacionesArchivadas();
    return ret;

}

DtInfoConversacion& ConversacionController::listarConversaciones(){

    return UsuarioController::getInstance()->getUsuario()->listarConversaciones();
}

void ConversacionController::seleccionarContacto(string numCelular){

    Usuario* user = UsuarioController::getInstance()->getUsuario()->seleccionarContacto(numCelular);
//    if (user == nullptr)
    this->memCont = user;
    this->memConv = nullptr;

}

list<DtMensaje *> ConversacionController::seleccionarConversacion(int idConv) {
    Usuario* user = UsuarioController::getInstance()->getUsuario();
    Conversacion* conv = this->colConversaciones[idConv];
    string numTelefono = user->getNumTelefono();

    auto convp = dynamic_cast<Privada *>(conv);
    if (convp != nullptr) {

        this->memConv = convp;
        this->memCont = nullptr;
        return convp->getDatosMensaje(user, numTelefono);
    }

    auto convg = dynamic_cast<Grupal *>(conv);
    if (convg != nullptr) {

        this->memConv = convg;
        this->memCont = nullptr;
        return convg->getDatosMensaje(user, numTelefono);
    }
}

void ConversacionController::seleccionarConversacionEnviar(int idConv){

    Conversacion* conv = this->colConversaciones[idConv];
    this->memConv = conv;
}

void ConversacionController::seleccionarMensajeParaEliminar(string codigo){

    Usuario* user = UsuarioController::getInstance()->getUsuario();
    Mensaje* msj = this->colMensajes[codigo];

    if (user->getColMensajesEnviados(codigo) != nullptr) {
        auto msjImg = dynamic_cast<Imagen *>(msj);
        if (msjImg != nullptr) { delete msjImg; }

        auto msjSim = dynamic_cast<Simple *>(msj);
        if (msjSim != nullptr) { delete msjSim; }

        auto msjVid = dynamic_cast<Video *>(msj);
        if (msjVid != nullptr) { delete msjVid; }

        auto msjCont = dynamic_cast<Contacto *>(msj);
        if (msjCont != nullptr) { delete msjCont; }

    } else if (user->getColUsMensaje(codigo) == nullptr) {
        user->eliminarMensaje(codigo);
    }

//    if (user->getColMensajesEnviados(codigo) != nullptr) {
//        this->memConv->desasignarMensaje(msj);
//        this->colMensajes.erase(codigo);
//        delete msj;
//    }
//
//    if (user->getColUsMensaje(codigo) != nullptr) {
//        user->eliminarMensaje(codigo);
//    }
}

list<DtInfoMensajeEnviado *> ConversacionController::verInfoMensajeEnviado(string codigo){

    Mensaje* msj = colMensajes[codigo];
    list<DtInfoMensajeEnviado *> ret = msj->getDatos();
    return ret;

}


///////Agregado a partir del caso de uso AltaGrupo

list<DtContacto *> ConversacionController::listarContactosSeleccionados(){
    list <DtContacto *> ret;
    for (auto &it : this->colContactosSeleccionados)
        ret.push_back(&it.second->getContacto());
    return ret;
}

list<DtContacto *> ConversacionController::listarContactosRestantes(){
    list<DtContacto*> ret;
    list<DtContacto*> aux = UsuarioController::getInstance()->getUsuario()->listarContactos();

    for (auto &it : aux) {
        // Si no se encuentra un usuario con numTelefono() en la coleccion de contactos seleccionados,
        // se agrega el DtContaco la lista de retorno.
        if (this->colContactosSeleccionados.find(it->getNumTelefono()) == this->colContactosSeleccionados.end())
            ret.push_back(it);
    }

//    for (auto &it : this->colContactosSeleccionados) {
//        if (it.second->getNumTelefono() ==
//    }
//
//        ret.remove(it.second->getNumTelefono());
//        ret.remove(it.getNumTelefono());

    return ret;
}

void ConversacionController::agregarUsuario(string numTelefono){

    if (this->colContactosSeleccionados.find(numTelefono)!= this->colContactosSeleccionados.end()){
        throw std::invalid_argument("El contacto ya fue seleccionado");
    }
    auto usuario = UsuarioController::getInstance()->getUsuario()->getColContactos(numTelefono);

    /*if(usuario != nullptr) {
        throw std::invalid_argument("El contacto ya fue seleccionado");
    } else{*/
        this->colContactosSeleccionados[numTelefono] = usuario;
    //}

}

void ConversacionController::quitarUsuario(string numTelefono){

    if (this->colContactosSeleccionados.empty()){
        throw std::invalid_argument("No hay un contacto seleccionado");
    }

    if (!this->colContactosSeleccionados.empty()) {
        this->colContactosSeleccionados.erase(numTelefono);
    }
}

void ConversacionController::crearGrupo(string nombre, string imagen, FechaHora fhCreacion) {

    if (this->colContactosSeleccionados.empty()){
        throw std::invalid_argument("Debe seleccionar al menos 1 contacto");
    }

    Usuario* usuario = UsuarioController::getInstance()->getUsuario();

    auto grupo = new Grupo(nombre, imagen,fhCreacion);
    auto convGrupal = new Grupal();
    convGrupal->setGrupo(grupo);

    auto usuarioGrupo = new UsuarioGrupo(true, fhCreacion, usuario, grupo);
    usuario->setColUsGrup(nombre, usuarioGrupo);

    for (auto &it : this->colContactosSeleccionados) {
        auto usuarioComunGrupo = new UsuarioGrupo(false, fhCreacion, it.second, grupo);
        grupo->setUsuarioGrupo(nombre,usuarioComunGrupo);
        it.second->setColUsGrup(nombre,usuarioComunGrupo);
        this->colContactosSeleccionados.erase(it.second->getNumTelefono());
    }
}

void ConversacionController::cancelar(){
    /*for (auto &it : this->colContactosSeleccionados)
        this->colContactosSeleccionados.erase(it.second->getNumTelefono());
        */
    this->colContactosSeleccionados.clear();
}

/*
//Getters

map<string, Mensaje *> ConversacionController::getColMensajes(){

    return colMensajes;
}

map<int, Conversacion *> ConversacionController::getColConversaciones(){

    return colConversaciones;
}

Usuario* ConversacionController::getMemCont(){

    return memCont;
}

Conversacion* ConversacionController::getMemConv(){

    return memConv;
}

//Setters
void ConversacionController::setColMensajes(map<string,Mensaje*>colectionMensaje){

    this->colMensajes = colectionMensaje;
}

void ConversacionController::setColConversaciones(map<int, Conversacion *> colectionConversacion){

    this->colConversaciones = colectionConversacion;
}

void ConversacionController::setMemCont(Usuario* us){

    this->memCont = us;
}

void ConversacionController::setMemConv(Conversacion * conv){

    this->memConv = conv;
}
*/
