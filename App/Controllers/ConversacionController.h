#ifndef CONVERSACIONCONTROLLER_H
#define CONVERSACIONCONTROLLER_H

#include <list>
#include <iostream>
#include <string>
#include <map>
#include "../Tipos.h"
#include "../DtContacto.h"
#include "../DtConversacion.h"
#include "../DtInfoConversacion.h"
#include "../DtInfoMensajeEnviado.h"
#include "../DtMensaje.h"
#include "../Mensaje.h"
#include "../Usuario.h"
#include "../Conversacion.h"
#include "../Interfaces/IConversacionController.h"
#include "UsuarioController.h"
#include "../FechaHora.h"
#include "../Imagen.h"
#include "../Contacto.h"
#include <time.h>
#include <sstream>
#include "../Reloj.h"
#include "../Grupo.h"
#include "../UsuarioGrupo.h"



using std::string;
using std::list;
using std::map;

class UsuarioController;
class ConversacionController: public IConversacionController {
    private:

        static ConversacionController* instance;
        ConversacionController(){};
        ConversacionController(ConversacionController const&){}
        map<string, Mensaje *> colMensajes;
        map<int, Conversacion *> colConversaciones;
        map <string, Usuario*> colContactosSeleccionados;
        Usuario* memCont;
        Conversacion* memConv;

    public:
        static ConversacionController* getInstance();
        void archivarConversacion(int id);
        void enviarMensajeContacto(string celular);
        void enviarMensajeImagen(string url, FormatoImg formato, int tam, string desc);
        void enviarMensajeSimple(string text);
        void enviarMensajeVideo(string url, int duracion);
        list<DtContacto *> listarContactos();
        list<DtConversacion *> listarConversacionesActivas();
        list<DtConversacion *> listarConversacionesArchivadas();
        DtInfoConversacion& listarConversaciones();
        void seleccionarContacto(string numCelular);
        list<DtMensaje *> seleccionarConversacion(int idConv);
        void seleccionarConversacionEnviar(int idConv);
        void seleccionarMensajeParaEliminar(string codigo);
        list<DtInfoMensajeEnviado *> verInfoMensajeEnviado(string codigo);
        list<DtContacto *> listarContactosSeleccionados();
        list<DtContacto *> listarContactosRestantes();
        void agregarUsuario( string numTelefono); //agregar a la coleccion que de contactos seleccionados
        void quitarUsuario(string numTelefono); //quitar de la coleccion de contactos seleccionados
        void crearGrupo(string nombre, string imagen, FechaHora fhCreacion); //crea el grupo y para cada elementos de la coleccion de contactos seleccionados se crea un usuarioGrupo que se agrega a la coleccion de usuarioGrupo
        void cancelar();

        //Getters
        map<string, Mensaje *> getColMensajes();
        map<int, Conversacion *> getColConversaciones();
        Usuario* getMemCont();
        Conversacion* getMemConv();

        //Setters
        void setColMensajes(map<string,Mensaje*>colectionMensaje);
        void setColConversaciones(map<int, Conversacion *> colectionConversacion);
        void setMemCont(Usuario* us);
        void setMemConv(Conversacion * conv);
};

#endif
