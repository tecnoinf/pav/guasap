#ifndef USUARIOCONTROLLER_H
#define USUARIOCONTROLLER_H

#include <list>
#include <map>
#include <string>
#include "../Interfaces/IUsuarioController.h"
#include "../Usuario.h"
#include "../FechaHora.h"

class Usuario;

class UsuarioController: public IUsuarioController {

    private:
        string memNumTelefono;
        Usuario* memNuevoContacto;
        Usuario* sesionActiva;
        map <string, Usuario*> colUsuarios;
        UsuarioController(){}
        UsuarioController(UsuarioController const&){}
        static UsuarioController* instance;

    public:
        static UsuarioController* getInstance();
        bool ingresarCelular(string numCelular);
        void asignarSesion(Usuario* user);
        void ingresarDatos(string nombre, string urlFoto, string descripcion);
        void ingresarDatosNombre(string nombre);
        void ingresarDatosUFoto(string urlFoto);
        void ingresarDatosDescripcion(string descripcion);
        void cancelar();
        list<DtInfoContacto> listarContactosDeUsuario();
        DtInfoContacto& indicarNumero(string numTelefono);
        void agregarContacto();
        void cerrarGuasap(FechaHora ultConexion);

        //Getters
        Usuario* getUsuario();
        /*string getMemNumTelefono();
        Usuario* getMemNuevoContacto();
        FechaHora getFechaSys();*/

        //Setters
    /*
        void setUsuario(Usuario* usuario);
        void setMemNumTelefono(string numTelefono);
        void setMemNuevoContacto(Usuario* nuevoContacto);
        void setFechaSys(FechaHora fechaSys);
        */

};

#endif // USUARIOCONTROLLER_H
