#include "UsuarioController.h"
#include "../Reloj.h"

UsuarioController* UsuarioController::instance = nullptr;


UsuarioController* UsuarioController::getInstance(){

    ///undefined reference to UsuarioController::UsuarioController
    if(instance==nullptr){
        instance = new UsuarioController();
    }

    return instance;
}


///HAY QUE VERIFICAR SI ESTA BIEN Y DE DONDE SACA LA HORA DEL SISTEMA EL CONTROLADOR
/*FechaHora UsuarioController::getFechaSys(){

    return fechaSys;
}*/


bool UsuarioController::ingresarCelular(string numCelular){
    this->memNumTelefono = numCelular;

    Usuario* usuario = colUsuarios[numCelular];
    if (usuario != nullptr) {
//        if (this->sesionActiva->getNumTelefono()!=numCelular){
//
//        }
        usuario->actualizarFhConexion(Reloj::getInstance()->dimeLaHora());
        asignarSesion(usuario);
        return true;
    }else {
        // Al buscar un elemento dentro del Map con corchetes, si el elemento no existe, se crea una entrada nueva
        // con la clave buscada, y valor NULL; por eso se hace el .erase() de la clave que se buscaba.
        // TODO: Hacer lo descrito arriba para todos los maps.
        colUsuarios.erase(numCelular);
        //throw std::invalid_argument("El usuario no existe.");
        return false;
    }
}
void UsuarioController::asignarSesion(Usuario* user){
    this->sesionActiva = user;
}

void UsuarioController::ingresarDatos(string nombre, string urlFoto, string descripcion) {
    FechaHora now = Reloj::getInstance()->dimeLaHora();

    Fecha fechaRegistro = Fecha(now.getDia(), now.getMes(), now.getAnio());
    Usuario * usuario = new Usuario(this->memNumTelefono, nombre, urlFoto, descripcion, fechaRegistro, now);
    colUsuarios[this->memNumTelefono] = usuario;
    asignarSesion(usuario);
}


void UsuarioController::ingresarDatosNombre(string nombre){

    this->sesionActiva->setNombre(nombre);
}

void UsuarioController::ingresarDatosUFoto(string urlFoto){

    this->sesionActiva->setImgPerfil(urlFoto);

}
void UsuarioController::ingresarDatosDescripcion(string descripcion){

    this->sesionActiva->setDescripcion(descripcion);
}

void UsuarioController::cancelar(){
    this->memNumTelefono = "";
}

list<DtInfoContacto> UsuarioController::listarContactosDeUsuario(){

    list <DtInfoContacto> ret = this->sesionActiva->listarContactosDeUsuario();
    return ret;
}

DtInfoContacto& UsuarioController::indicarNumero(string numTelefono){
    Usuario* usuario = colUsuarios[numTelefono];
    if (usuario != nullptr){
        this->memNuevoContacto = usuario;
        this->memNumTelefono = numTelefono;
        return usuario->getInfoContacto();
    }
    else {
        // Al buscar un elemento dentro del Map con corchetes, si el elemento no existe, se crea una entrada nueva
        // con la clave buscada, y valor NULL; por eso se hace el .erase() de la clave que se buscaba.
        // TODO: Hacer lo descrito arriba para todos los maps.
        colUsuarios.erase(numTelefono);
        throw std::invalid_argument("El usuario no existe.");
    }
}

void UsuarioController::agregarContacto(){
    this->sesionActiva->setColContactos(this->memNumTelefono, this->memNuevoContacto);
    this->memNuevoContacto = nullptr;
    this->memNumTelefono = "";
}

void UsuarioController::cerrarGuasap(FechaHora ultConexion) {
    this->sesionActiva->actualizarFhConexion(ultConexion);
    this->sesionActiva = nullptr;
}

//Getters

Usuario* UsuarioController::getUsuario(){

    return sesionActiva;
}
/*
string UsuarioController::getMemNumTelefono(){

    return memNumTelefono;
}
Usuario* UsuarioController::getMemNuevoContacto(){

    return memNuevoContacto;
}

//Setters
void UsuarioController::setUsuario(Usuario* usuario){

    this->sesionActiva = usuario;
}
void UsuarioController::setMemNumTelefono(string numTelefono){

    this->memNumTelefono = numTelefono;
}

void UsuarioController::setMemNuevoContacto(Usuario* nuevoContacto){

    this->memNuevoContacto = nuevoContacto;
}
 */

