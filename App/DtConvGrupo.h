
#ifndef GUASAP_DTCONVGRUPO_H
#define GUASAP_DTCONVGRUPO_H

#include "DtConversacion.h"
#include <string>

using std::string;

class DtConvGrupo :public DtConversacion{

    private:
        string nomGrupo;

    public:
        //Constructores
        DtConvGrupo();
        DtConvGrupo(int id, string nomGrupo);

        //Getters
        string getNomGrupo();
};

#endif //GUASAP_DTCONVGRUPO_H
