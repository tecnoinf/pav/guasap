//
// Created by seba on 09/06/18.
//
#include "UsuarioGrupo.h"
#include "Grupo.h"
#include "FechaHora.h"
#include <string>

using namespace std;

UsuarioGrupo::UsuarioGrupo()
{
    this->esAdmin=false;
    this->fhMiembro=FechaHora();
    this->usuario= nullptr;
    this->grupo= nullptr;
}

UsuarioGrupo::UsuarioGrupo(bool esAdmin, FechaHora fhMiembro, Usuario* usuario, Grupo* grupo)
{
    this->esAdmin=esAdmin;
    this->fhMiembro=fhMiembro;
    this->usuario=usuario;
    this->grupo=grupo;
}

bool UsuarioGrupo::getEsAdmin()
{
    return esAdmin;
}

FechaHora UsuarioGrupo::getFhMiembro()
{
    return fhMiembro;
}

Usuario* UsuarioGrupo::getUsuario()
{
    return usuario;
}

Grupo* UsuarioGrupo::getGrupo()
{
    return grupo;
}

void UsuarioGrupo::setEsAdmin(bool esAdmin)
{
    this->esAdmin=esAdmin;
}

void UsuarioGrupo::setFhMiembro(FechaHora fhMiembro)
{
    this->fhMiembro=fhMiembro;
}

void UsuarioGrupo::setUsuario(Usuario* usuario)
{
    this->usuario=usuario;
}

void UsuarioGrupo::setGrupo(Grupo* grupo)
{
    this->grupo=grupo;
}

UsuarioGrupo::~UsuarioGrupo()
{
    if (this->esAdmin) {
        delete grupo;
    }
}
