//
// Created by sebiitta on 09/06/18.
//

#ifndef GUASAP_DTMSJIMAGEN_H
#define GUASAP_DTMSJIMAGEN_H


#include "DtMensaje.h"
#include "Tipos.h"

class DtMsjImagen: public DtMensaje {
    private:
        int tamanio;
        string texto;
        string url;
        FormatoImg formato;

    public:
        DtMsjImagen();
        DtMsjImagen(int tamanio, string texto, string url, FormatoImg formato, string codigo, FechaHora fhEnviado);

        int getTamanio();
        string getTexto();
        string getUrl();
        FormatoImg getFormato();
};


#endif //GUASAP_DTMSJIMAGEN_H
