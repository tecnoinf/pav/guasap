//
// Created by sebiitta on 09/06/18.
//

#ifndef GUASAP_DTMSJCONTACTO_H
#define GUASAP_DTMSJCONTACTO_H


#include "DtMensaje.h"

class DtMsjContacto: public DtMensaje {
    private:
        string nombre;
        string numTelefono;

    public:
        DtMsjContacto();
        DtMsjContacto(string nombre, string numTelefono, string codigo, FechaHora fhEnviado);

        string getNombre();
        string getNumTelefono();
};


#endif //GUASAP_DTMSJCONTACTO_H
