
#include "Imagen.h"
#include "DtMsjImagen.h"
#include "string"
#include "Tipos.h"
#include "Multimedia.h"

using namespace std;

Imagen::Imagen()
{
    this->formato=jpg;
    this->tamanio=0;
    this->texto="";
    this->url="";
}

Imagen::Imagen(string codigo, FechaHora fhEnviado, FormatoImg formato, int tamanio, string texto, string url): Multimedia(codigo, fhEnviado)
{
    this->formato=formato;
    this->tamanio=tamanio;
    this->texto=texto;
    this->url=url;
}

//Getters
FormatoImg Imagen::getFormato()
{
    return formato;
}

int Imagen::getTamanio()
{
    return tamanio;
}
string Imagen::getTexto()
{
    return texto;
}

string Imagen::getUrl()
{
    return url;
}
//Setters
void Imagen::setFormatoImg(FormatoImg formato)
{
    this->formato=formato;
}

void Imagen::setTamanio(int tamanio)
{
    this->tamanio=tamanio;
}

void Imagen::setTexto(string texto)
{
    this->texto=texto;
}

void Imagen::setUrl(string url)
{
    this->url=url;
}

//Operaciones
DtMensaje& Imagen::getDatos(string tel,Usuario* us)
{
    auto dtMsImg = new DtMsjImagen(getTamanio(), getTexto(), getUrl(), getFormato(), getCodigo(), getFhEnviado());
    return *dtMsImg;
}


//Destructor
Imagen::~Imagen()=default;
