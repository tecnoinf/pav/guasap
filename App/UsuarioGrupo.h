//
// Created by seba on 09/06/18.
//

#ifndef GUASAP_USUARIOGRUPO_H
#define GUASAP_USUARIOGRUPO_H

#include "FechaHora.h"
#include "Usuario.h"
#include "Grupo.h"

class Usuario;

class Grupo;

class UsuarioGrupo{

    private:
        bool esAdmin;
        FechaHora fhMiembro;
        Usuario* usuario;
        Grupo* grupo;

    public:
        UsuarioGrupo();
        UsuarioGrupo(bool esAdmin, FechaHora fhMiembro, Usuario* usuario, Grupo* grupo);

        bool getEsAdmin();
        FechaHora getFhMiembro();
        Usuario* getUsuario();
        Grupo* getGrupo();

        void setEsAdmin(bool esAdmin);
        void setFhMiembro(FechaHora fhMiembro);
        void setUsuario(Usuario* usuario);
        void setGrupo(Grupo* grupo);

        ~UsuarioGrupo();
};

#endif //GUASAP_USUARIOGRUPO_H
