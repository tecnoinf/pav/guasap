
#include "UsuarioMensaje.h"
#include "FechaHora.h"

UsuarioMensaje::UsuarioMensaje(){

    this->visto = false;
    this->eliminado = false;
    this->fhVisto = FechaHora();
    this->receptor = nullptr;
    this->msj = nullptr;
}

UsuarioMensaje::UsuarioMensaje(bool visto, bool eliminado, FechaHora fhVisto){

    this->visto = visto;
    this->eliminado = eliminado;
    this->fhVisto = fhVisto;
}

bool UsuarioMensaje::getVisto(){

    return visto;
    }


bool UsuarioMensaje::getEliminado(){

    return eliminado;
}


FechaHora UsuarioMensaje::getfhVisto(){

    return fhVisto;
}

DtInfoMensajeEnviado& UsuarioMensaje::getDatos(){

    string nombContac = this->getReceptor()->getNombre();
    string TeleContac = this->getReceptor()->getNumTelefono();
    FechaHora fechaHoraEnv = this->getfhVisto();

    auto dtInfoMensEnvi = new DtInfoMensajeEnviado(nombContac, TeleContac, fechaHoraEnv);

    return *dtInfoMensEnvi;
}

Usuario* UsuarioMensaje::getReceptor(){
    return this->receptor;
}

Mensaje* UsuarioMensaje::getMsj() {
    return this->msj;
}



void UsuarioMensaje::setVisto(bool visto){

    this->visto = visto;
}

void UsuarioMensaje::setEliminado(bool eliminado){

    this->eliminado = eliminado;
}

void UsuarioMensaje::setFhVisto(FechaHora fhVisto){

    this->fhVisto = fhVisto;
}

UsuarioMensaje::~UsuarioMensaje() = default;



