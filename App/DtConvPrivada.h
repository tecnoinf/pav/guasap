
#ifndef GUASAP_DTCONVPRIVADA_H
#define GUASAP_DTCONVPRIVADA_H

#include "DtConversacion.h"
#include <string>

//using std::string;

class DtConvPrivada: public DtConversacion {

private:
    std::string numTelefono, nombre;

public:
    //Constructores
    DtConvPrivada();
    DtConvPrivada(int id, std::string numTelefono, std::string nombre);

    //Getters
    std::string getNumTelefono();
    std::string getNombre();
};


#endif //GUASAP_DTCONVPRIVADA_H
