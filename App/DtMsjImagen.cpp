//
// Created by sebiitta on 09/06/18.
//

#include "DtMsjImagen.h"

DtMsjImagen::DtMsjImagen(): DtMensaje() {
    this->tamanio = 0;
    this->texto = "";
    this->url = "";
//    this->formato;
}

DtMsjImagen::DtMsjImagen(int tamanio, string texto, string url, FormatoImg formato, string codigo, FechaHora fhEnviado): DtMensaje(codigo, fhEnviado) {
    this->tamanio = tamanio;
    this->texto = texto;
    this->url = url;
//    this->formato = formato;
}

int DtMsjImagen::getTamanio() {
    return this->tamanio;
}

string DtMsjImagen::getTexto() {
    return this->texto;
}

string DtMsjImagen::getUrl() {
    return this->url;
}

FormatoImg DtMsjImagen::getFormato() {
    return this->formato;
}
