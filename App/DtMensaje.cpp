//
// Created by sebiitta on 09/06/18.
//

#include "DtMensaje.h"

DtMensaje::DtMensaje() {
    this->codigo = "";
    this->fhEnviado = FechaHora();
}

DtMensaje::DtMensaje(string codigo, FechaHora fhEnviado) {
    this->codigo = codigo;
    this->fhEnviado = fhEnviado;
}

string DtMensaje::getCodigo() {
    return this->codigo;
}

FechaHora DtMensaje::getFhEnviado() {
    return this->fhEnviado;
}

DtMensaje::~DtMensaje() = default;

std::ostream& operator<<(std::ostream& out, DtMensaje& dtm) {
    return out << dtm.getCodigo() << std::endl;
}
