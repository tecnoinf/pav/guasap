//
// Created by sebiitta on 09/06/18.
//

#ifndef GUASAP_DTMSJVIDEO_H
#define GUASAP_DTMSJVIDEO_H


#include "DtMensaje.h"

class DtMsjVideo: public DtMensaje {
    private:
        int duracion;
        string url;

    public:
        DtMsjVideo();
        DtMsjVideo(int duracion, string url, string codigo, FechaHora fhEnviado);

        int getDuracion();
        string getUrl();
};


#endif //GUASAP_DTMSJVIDEO_H
