//
// Created by sebiitta on 09/06/18.
//

#include "DtMsjVideo.h"

DtMsjVideo::DtMsjVideo(): DtMensaje() {
    this->duracion = 0;
    this->url = "";
}

DtMsjVideo::DtMsjVideo(int duracion, string url, string codigo, FechaHora fhEnviado): DtMensaje(codigo, fhEnviado) {
    this->duracion = duracion;
    this->url = url;
}

int DtMsjVideo::getDuracion() {
    return this->duracion;
}

string DtMsjVideo::getUrl() {
    return this->url;
}
