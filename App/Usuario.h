#ifndef GUASAP_USUARIO_H
#define GUASAP_USUARIO_H

#include "Conversacion.h"
#include "Fecha.h"
#include "FechaHora.h"
#include "UsuarioConversacion.h"
#include "UsuarioGrupo.h"
#include "Mensaje.h"
#include "UsuarioMensaje.h"
#include "DtInfoContacto.h"
#include "DtContacto.h"
#include "DtConversacion.h"
#include "DtInfoContacto.h"
#include "DtInfoConversacion.h"
#include <map>
#include <list>
#include <string>

class UsuarioConversacion;
class Conversacion;

class UsuarioGrupo;

class Usuario{

    private:
        string numTelefono, nombre, imgPerfil, descripcion;
        Fecha fechaRegistro;
        FechaHora ultConexion;
        map <int,UsuarioConversacion*> colUsCon;
        map <string, UsuarioGrupo*> colUsGrup;
        map <string, Usuario*> colContactos;
        map <string, Mensaje*> colMensajesEnviados;
        map <string, UsuarioMensaje*> colUsMensaje;

    public:

        //Constructores

        Usuario();
        Usuario(string numTelefono,string nombre, string imgPerfil, string descripcion, Fecha fechaRegistro, FechaHora ultConexion);

        //Getters

        string getNumTelefono();
        string getNombre();
        string getImgPerfil();
        string getDescripcion();
        Fecha getFechaRegistro();
        FechaHora getUltConexion();
        UsuarioConversacion* getColUsCon(int id);
        UsuarioGrupo* getColUsGrup(string clave);
        Usuario* getColContactos(string clave);
        Mensaje* getColMensajesEnviados(string clave);
        UsuarioMensaje* getColUsMensaje(string clave);

        //Setters

        void setNumTelefono(string numTelefono);
        void setNombre(string nombre);
        void setImgPerfil(string imgPerfil);
        void setDescripcion(string descripcion);
        void setFechaRegistro(Fecha fechaRegistro);
        void setUltConexion(FechaHora ultConexion);
        void setColUsCon(int clave,UsuarioConversacion* usCon);
        void setColUsGrup(string clave,UsuarioGrupo* usGrup);
        void setColContactos(string clave, Usuario* usuario);
        void setColMensajesEnviados(string clave, Mensaje* msj);
        void setColUsMensaje(string clave, UsuarioMensaje* usMensaje);

        //Operaciones
        void actualizarFhConexion(FechaHora fechaSys);
        void agregarContacto(Usuario* nuevoCont);
        void archivarConversacion(int id);
        Usuario* contactoAEnviar(string numCelular);
        void eliminarMensaje(string codigo);
        DtInfoContacto& getInfoContacto();
        DtContacto& getContacto();
        UsuarioConversacion* iniciarConversacion(Conversacion* conv);
        list <DtContacto*> listarContactos();
        list <DtInfoContacto> listarContactosDeUsuario();
        list <DtConversacion*> listarConversacionesActivas();
        list <DtConversacion*> listarConversacionesArchivadas();
        DtInfoConversacion& listarConversaciones();
        Usuario* seleccionarContacto(string numCeluar);
        void desasignarUMensaje(UsuarioMensaje uem);

        //Destructores
        ~Usuario();

};







#endif //GUASAP_USUARIO_H
