//
// Created by sebiitta on 10/06/18.
//

#ifndef GUASAP_SIMPLE_H
#define GUASAP_SIMPLE_H

#include "DtMensaje.h"
#include "Usuario.h"


class Simple: public Mensaje {
    private:
        string texto;

    public:
        Simple();
        Simple(string codigo, FechaHora fhEnviado, string texto);

        DtMensaje& getDatos(string tel, Usuario* us);

        // Getters
        string getTexto();

        ~Simple();
};


#endif //GUASAP_SIMPLE_H
