//
// Created by seba on 09/06/18.
//
#include <string>
#include "Grupo.h"
#include "FechaHora.h"

using namespace std;

Grupo::Grupo() {
    this->nombre="";
    this->imagen="";
    this->fhCreacion=FechaHora();
}

Grupo::Grupo(string nombre, string imagen, FechaHora fhCreacion) {
    this->nombre=nombre;
    this->imagen=imagen;
    this->fhCreacion=fhCreacion;
}


string Grupo::getNombre()
{
    return nombre;
}

string Grupo::getImagen()
{
    return imagen;
}

FechaHora Grupo::getCreacion()
{
    return fhCreacion;
}

UsuarioGrupo* Grupo::getUsuarioGrupo(string clave)
{
    return colUsuarioGrupo[clave];
}

void Grupo::setNombre(string nombre)
{
    this->nombre=nombre;
}

void Grupo::setImagen(string imagen)
{
    this->imagen=imagen;
}

void Grupo::setFhCreacion(FechaHora fhCreacion)
{
    this->fhCreacion=fhCreacion;
}

void Grupo::setUsuarioGrupo(string clave, UsuarioGrupo* usuarioGrupo)
{
   this->colUsuarioGrupo[clave]=usuarioGrupo;
}

FechaHora Grupo::getFhMiembro(string tel)
{
    UsuarioGrupo* usGrupo=getUsuarioGrupo(tel);
    return usGrupo->getFhMiembro();
}

Grupo::~Grupo()=default;

