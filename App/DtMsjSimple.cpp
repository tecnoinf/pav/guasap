//
// Created by sebiitta on 09/06/18.
//

#include "DtMsjSimple.h"

DtMsjSimple::DtMsjSimple(): DtMensaje() {
    this->texto = "";
}

DtMsjSimple::DtMsjSimple(string texto, string codigo, FechaHora fhEnviado): DtMensaje(codigo, fhEnviado) {
    this->texto = texto;
}

string DtMsjSimple::getTexto() {
    return this->texto;
}
