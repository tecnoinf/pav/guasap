//
// Created by sebiitta on 10/06/18.
//

#include "Contacto.h"




Contacto::Contacto():Mensaje(){

    this->usuarioAsociado = nullptr;
}

Contacto::Contacto(string codigo, FechaHora fhEnviado):Mensaje(codigo, fhEnviado){

    this->usuarioAsociado = nullptr;
}

void Contacto::agregarContactoAMsj(Usuario* aenviar){

    this->usuarioAsociado = aenviar;
}

DtMensaje& Contacto::getDatos(string tel, Usuario* us){

    Usuario* usuAEnviar = us->getColContactos(tel);
    getUsuarioMensaje(tel)->setVisto(true);
    auto dtm = new DtMsjContacto(usuAEnviar->getNombre(), usuAEnviar->getNumTelefono() , this->getCodigo(), this->getFhEnviado());
    return *dtm;
}

Contacto::~Contacto() = default;
