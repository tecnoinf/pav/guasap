#ifndef GUASAP_USUARIOCONVERSACION_H
#define GUASAP_USUARIOCONVERSACION_H

#include "Usuario.h"
#include "Conversacion.h"
#include "DtConversacion.h"

class Usuario;
class Conversacion;

class UsuarioConversacion{

    private:
        Usuario* usuario;
		Conversacion* conversacion;
        bool estaActiva;

    public:
        //Contructores

        UsuarioConversacion();
        UsuarioConversacion(bool estaActiva, Usuario* usuario, Conversacion* conversacion);

        //Getters

        bool getEstaActiva();
        Usuario* getUsuario();
		Conversacion* getConversacion();

        //Setters

        void setEstaActiva(bool activa);
        void setUsuario(Usuario* usuario);
		void setConversacion(Conversacion* conversacion);

        //Operaciones

        void addConversacion(Conversacion* conv);
        void archivar();
        DtConversacion* getDatosConversacion();

        //Destructor
        ~UsuarioConversacion();
};




#endif //GUASAP_USUARIOCONVERSACION_H
