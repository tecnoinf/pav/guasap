#include "Conversacion.h"
#include "Grupal.h"
#include "Grupo.h"
#include "Usuario.h"
#include "DtMensaje.h"
#include "DtConversacion.h"
#include "DtConvGrupo.h"
#include "FechaHora.h"
#include <string>
#include <list>


//Constructores

Grupal::Grupal():Conversacion(){
    this->grupo = nullptr;
}

Grupal::Grupal(int id, Grupo* grupo):Conversacion(id){
    this->grupo = grupo;
}

//Getters

Grupo* Grupal::getGrupo(){
    return grupo;
}

//Setters

void Grupal::setGrupo(Grupo* grupo){
    this->grupo = grupo;
}

//Operaciones

DtConversacion* Grupal::getDatos() {

    Grupo *grupo = getGrupo();
    string nombre = grupo->getNombre();
    DtConvGrupo* dtConv = new DtConvGrupo(getId(), nombre);

    return dtConv;
}

list <DtMensaje *> Grupal::getDatosMensaje(Usuario* us, string tel) {

    list<DtMensaje *> listDtMensajes;
    FechaHora fh = getGrupo()->getFhMiembro(tel);

    cout << "    Antes de iterar sobre this->colMensajes <----- Grupal" << endl;
    for (auto &it: this->colMensajes) {
        FechaHora fhm = it.second->getFhEnviado();
        if (fh > fhm) {

            // TODO: Rvisar si hay que hcer dynamicCast para saber que tipo de mensaje es...
            cout << "    Antes de llamar a it.second->getDatos(tel, us); <----- Grupal" << endl;
            auto dtm = it.second->getDatos(tel, us);
            listDtMensajes.push_back(&dtm);
        }
    }

    return listDtMensajes;
}

//Destructor
Grupal::~Grupal() {
    delete grupo;
}
