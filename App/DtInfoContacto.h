#ifndef GUASAP_DTINFOCONTACTO_H
#define GUASAP_DTINFOCONTACTO_H
#include <string>
#include "DtContacto.h"

class DtInfoContacto :public DtContacto{

    private:
        string imgPerfil;

    public:
        //Constructores
        DtInfoContacto();
        DtInfoContacto(string numTelefono, string nombre, string imgPerfil);

        //Getters
        string getImgPerfil();
};

std::ostream& operator <<(std::ostream& out, DtInfoContacto& dtIC);

#endif //GUASAP_DTINFOCONTACTO_H
