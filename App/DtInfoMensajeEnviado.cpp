

#include "DtInfoMensajeEnviado.h"

#include <string>
using std::string;

//Constructor
DtInfoMensajeEnviado::DtInfoMensajeEnviado() {

    this->nombreReceptor = "";
    this->numTelReceptor = "";
    this->horaVisto = FechaHora();
}

DtInfoMensajeEnviado::DtInfoMensajeEnviado(string nombreReceptor, string numTelReceptor, FechaHora horaVisto) {

    this->nombreReceptor = nombreReceptor;
    this->numTelReceptor = numTelReceptor;
    this->horaVisto = horaVisto;
}

//Getters
string DtInfoMensajeEnviado::getNombreReceptor(){

    return nombreReceptor;
}

string DtInfoMensajeEnviado::getNumTelReceptor(){

    return numTelReceptor;
}

FechaHora DtInfoMensajeEnviado::getHoraVisto(){

    return horaVisto;
}
