//
// Created by seba on 09/06/18.
//

#ifndef GUASAP_DTINFOCONVERSACION_H
#define GUASAP_DTINFOCONVERSACION_H

#include "DtConversacion.h"
#include <iostream>
#include <list>

using namespace std;

class DtInfoConversacion {
private:
    int convArch;
    list <DtConversacion*> convActivas;

public:
    DtInfoConversacion();
    DtInfoConversacion(int convArch, list <DtConversacion*> convActivas);
    int getConvArch();
    list <DtConversacion*> getConvActivas();
};

std::ostream& operator <<(std::ostream& out, DtInfoConversacion& dtIC);

#endif //GUASAP_DTINFOCONVERSACION_H
