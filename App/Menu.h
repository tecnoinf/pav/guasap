//
// Created by sebiitta on 23/06/18.
//

#ifndef GUASAP_MENU_H
#define GUASAP_MENU_H


#include "Interfaces/IUsuarioController.h"
#include "Interfaces/IConversacionController.h"
#include "Factories/FConversacionController.h"
#include "Factories/FUsuarioController.h"
#include "Reloj.h"
#include "DtInfoContacto.h"

class Menu {
    private:
        // Instancia de Menu.
        static Menu* instance;
        bool loggedIn;

        // Constructor.
        Menu();

        // Interfases y fabricas
        IUsuarioController* iuc;
        FUsuarioController* fuc;
        IConversacionController* icc;
        FConversacionController* fcc;

        // Reloj.
        Reloj* rlj;

    public:
        // Obtener la instancia del menu.
        static Menu* getInstance();

        // Impresion e interaccion del menu.
        void printMenu();
        void mainMenu();

        // Getters y Setters
        bool getLoggedIn();
        void setLoggedIn(bool log);

        // Manejador de las opciones.
        void handleOption1();
        void handleOption2();
        void handleOption3();
        void handleOption4();
        void handleOption5();
        void handleOption6();
        void handleOption7();
        void handleOption8();
        void handleOption9();
        void handleOption10();
        void handleOption11();
        void handleOption12();


        // Exception Handler
        void handleExceptions(const string &eWhat);

        // Destructor
        ~Menu();
};


#endif //GUASAP_MENU_H
