#ifndef DTINFOMENSAJEENVIADO_H_INCLUDED
#define DTINFOMENSAJEENVIADO_H_INCLUDED

#include <string>
#include "FechaHora.h"

using std::string;

class DtInfoMensajeEnviado{

    private:
        string nombreReceptor;
        string numTelReceptor;
        FechaHora horaVisto;

    public:

        //Constructor
        DtInfoMensajeEnviado();
        DtInfoMensajeEnviado(string nombreReceptor, string numTelReceptor, FechaHora horaVisto);

        //Getters
        string getNombreReceptor();
        string getNumTelReceptor();
        FechaHora getHoraVisto();
};

#endif // DTINFOMENSAJEENVIADO_H_INCLUDED
