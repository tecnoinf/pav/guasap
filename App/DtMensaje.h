//
// Created by sebiitta on 09/06/18.
//

#ifndef GUASAP_DTMENSAJE_H
#define GUASAP_DTMENSAJE_H

#include <string>
#include "FechaHora.h"
#include <iostream>

using std::string;

class DtMensaje {
    private:
        string codigo;
        FechaHora fhEnviado;

    public:
        DtMensaje();
        DtMensaje(string codigo, FechaHora fhEnviado);

        string getCodigo();
        FechaHora getFhEnviado();

        virtual ~DtMensaje();
};

std::ostream& operator<<(std::ostream& out, DtMensaje& dtm);


#endif //GUASAP_DTMENSAJE_H
