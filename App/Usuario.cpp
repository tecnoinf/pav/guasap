#include "Usuario.h"
#include <string>
#include "Mensaje.h"
//Constructores

Usuario::Usuario(){
    this->numTelefono = "";
    this->nombre = "";
    this->imgPerfil = "";
    this->descripcion = "";
    this->fechaRegistro = Fecha();
    this->ultConexion = FechaHora();
}
Usuario::Usuario(string numTelefono,string nombre, string imgPerfil, string descripcion, Fecha fechaRegistro, FechaHora ultConexion){
    this->numTelefono = numTelefono;
    this->nombre = nombre;
    this->imgPerfil = imgPerfil;
    this->descripcion = descripcion;
    this->fechaRegistro = fechaRegistro;
    this->ultConexion = ultConexion;
}

//Getters

string Usuario::getNumTelefono(){
    return numTelefono;
}

string Usuario::getNombre(){
    return nombre;
}

string Usuario::getImgPerfil(){
    return imgPerfil;
}

string Usuario::getDescripcion(){
    return descripcion;
}
Fecha Usuario::getFechaRegistro(){
    return fechaRegistro;
}
FechaHora Usuario::getUltConexion(){
    return ultConexion;
}

UsuarioConversacion* Usuario::getColUsCon(int id){
    return colUsCon[id];
}

UsuarioGrupo* Usuario::getColUsGrup(string clave){
    return colUsGrup[clave];
}

Usuario* Usuario::getColContactos(string clave){
    // TODO: Si "clave" no existe (o sea es NULL), habria que eliminar la entrada (o cambiar el metodo de retorno de elementos).
    auto user = colContactos[clave];
    return user;
}

Mensaje* Usuario::getColMensajesEnviados(string clave){
    return colMensajesEnviados[clave];
    //return ret;
}

UsuarioMensaje* Usuario::getColUsMensaje(string clave){
    return colUsMensaje[clave];
}

//Setters

void Usuario::setNumTelefono(string numTelefono){
    this->numTelefono = numTelefono;
}
void Usuario::setNombre(string nombre){
    this->nombre = nombre;
}
void Usuario::setImgPerfil(string imgPerfil){
    this->imgPerfil = imgPerfil;
}
void Usuario::setDescripcion(string descripcion){
    this->descripcion = descripcion;
}
void Usuario::setFechaRegistro(Fecha fechaRegistro){
    this->fechaRegistro = fechaRegistro;
}
void Usuario::setUltConexion(FechaHora ultConexion){
    this->ultConexion = ultConexion;
}
void Usuario::setColUsCon(int clave,UsuarioConversacion* usCon){
    this->colUsCon[clave] = usCon;
}
void Usuario::setColUsGrup(string clave,UsuarioGrupo* usGrup){
    this->colUsGrup[clave] = usGrup;
}
void Usuario::setColContactos(string clave, Usuario* usuario){
    if (this->colContactos[clave] != nullptr){
        throw std::invalid_argument("El numero ingresado ya es esta agregado como contacto.");
    }
    this->colContactos[clave] = usuario;
}
void Usuario::setColMensajesEnviados(string clave, Mensaje* msj){
    this->colMensajesEnviados[clave] = msj;
}
void Usuario::setColUsMensaje(string clave, UsuarioMensaje* usMensaje){
    this->colUsMensaje[clave] = usMensaje;
}

//Operaciones

void Usuario::actualizarFhConexion(FechaHora fechaSys){
    setUltConexion(fechaSys);
}

void Usuario::agregarContacto(Usuario* nuevoCont){
    string clave = nuevoCont->getNumTelefono();
    setColContactos(clave, nuevoCont);
}

void Usuario::archivarConversacion(int id){
    UsuarioConversacion* usCon = getColUsCon(id);
    usCon->setEstaActiva(false);
}

Usuario* Usuario::contactoAEnviar(string numCelular){
    return getColContactos(numCelular);
}

void Usuario::eliminarMensaje(string codigo){
    UsuarioMensaje* usuarioMensaje = getColUsMensaje(codigo);
    colUsMensaje.erase(codigo);
    usuarioMensaje->getMsj()->desasignarUMensaje(usuarioMensaje);
    delete usuarioMensaje;
}

DtInfoContacto& Usuario::getInfoContacto(){
    DtInfoContacto* dtInfoContacto = new DtInfoContacto(getNumTelefono(),getNombre(),getImgPerfil());
    return *dtInfoContacto;
}

DtContacto& Usuario::getContacto(){
    DtContacto* dtContacto = new DtContacto(getNumTelefono(),getNombre());
    return *dtContacto;
}

UsuarioConversacion* Usuario::iniciarConversacion(Conversacion* conv){
    UsuarioConversacion* usCon = new UsuarioConversacion();
    int clave = conv->getId();
    setColUsCon(clave, usCon);
    usCon->setEstaActiva(true);
    usCon->setUsuario(this);
    usCon->addConversacion(conv);
    conv->asignarConversacion(usCon);

    return usCon;
}

list <DtContacto *> Usuario::listarContactos(){
    list <DtContacto *> ret;

    for (auto &it : this->colContactos)
        ret.push_back(&it.second->getContacto());

    return ret;
}

list <DtInfoContacto> Usuario::listarContactosDeUsuario(){
    list <DtInfoContacto> ret;
    for (auto &it : this->colContactos)
        ret.push_back(it.second->getInfoContacto());
    return ret;
}

list <DtConversacion*> Usuario::listarConversacionesActivas(){
    list <DtConversacion*> ret;
    for (auto &it : this->colUsCon)
        if (it.second->getEstaActiva() == true)
            ret.push_back(it.second->getDatosConversacion());
    return ret;
}

list <DtConversacion*> Usuario::listarConversacionesArchivadas(){
    list <DtConversacion*> ret;
    for (auto &it : this->colUsCon) {
        if (it.second->getEstaActiva() == false)
            ret.push_back(it.second->getDatosConversacion());
    }
    return ret;
}

DtInfoConversacion& Usuario::listarConversaciones(){

    int cont = 0;
    list <DtConversacion*> listDtConv;
    for (auto &it : this->colUsCon) {
        if (it.second->getEstaActiva() == true)
            listDtConv.push_back(it.second->getDatosConversacion());
        else
            cont++;
    }
    DtInfoConversacion* ret = new DtInfoConversacion(cont, listDtConv);
    return *ret;
}

Usuario* Usuario::seleccionarContacto(string numCeluar){
    return getColContactos(numCeluar);
}

void Usuario::desasignarUMensaje(UsuarioMensaje uem){
    string codigo = uem.getMsj()->getCodigo();
    colUsMensaje.erase(codigo);
}

//Destructores
Usuario::~Usuario(){
    for (auto &it : this->colUsCon) {
        this->colUsCon.erase(it.second->getConversacion()->getId());
        delete it.second;
    }

    for (auto &it : this->colMensajesEnviados) {
        this->colMensajesEnviados.erase(it.second->getCodigo());
        //delete  it.second;
    }

    for (auto &it : this->colContactos) {
        this->colContactos.erase(it.second->getNumTelefono());
    }

    for (auto &it : this->colUsGrup) {
        this->colUsGrup.erase(it.second->getGrupo()->getNombre());
        delete it.second;
    }

    for (auto &it : this->colUsMensaje) {
        this->colUsMensaje.erase(it.second->getMsj()->getCodigo());
        delete it.second;
    }

/*

    map <string, UsuarioMensaje*> colUsMensaje;
    */
}
