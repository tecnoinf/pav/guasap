//
// Created by seba on 09/06/18.
//

#include "DtInfoConversacion.h"
#include <list>

using namespace std;

DtInfoConversacion::DtInfoConversacion()
{
    this->convArch=0;
}

DtInfoConversacion::DtInfoConversacion(int convArch, list <DtConversacion*> convActivas)
{
    this->convArch=convArch;
    this->convActivas=convActivas;
}
int DtInfoConversacion::getConvArch()
{
    return convArch;
}

list <DtConversacion*> DtInfoConversacion::getConvActivas()
{
    return convActivas;
}

ostream& operator <<(ostream& out, DtInfoConversacion& dtIC) {

    auto convActivas = dtIC.getConvActivas();

    if (convActivas.empty()) {
        out << "No hay conversaciones activas." << endl;

    } else {
        out << "Conversaciones:" << endl;
        for (auto &it : convActivas) {
            out << "   Conv ID: " << it->getId() << endl;
        }
    }

    return out << "[Archivadas]: " << dtIC.getConvArch() << endl;
}
