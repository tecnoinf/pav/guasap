//
// Created by sebiitta on 08/06/18.
//

#ifndef GUASAP_IUSUARIOCONTROLLER_H
#define GUASAP_IUSUARIOCONTROLLER_H
#include <string>
#include "../DtInfoContacto.h"
#include "../FechaHora.h"
#include <list>

using namespace std;

class IUsuarioController {
    public:
        virtual bool ingresarCelular(string numCelular) = 0;
        virtual void ingresarDatos(string nombre, string urlFoto, string descripcion) = 0;
        virtual void ingresarDatosNombre(string urlFoto) = 0;
        virtual void ingresarDatosUFoto(string nombre) = 0;
        virtual void ingresarDatosDescripcion(string nombre) = 0;
        virtual void cancelar() = 0;
        virtual list <DtInfoContacto> listarContactosDeUsuario() = 0;
        virtual DtInfoContacto& indicarNumero(string numTelefono) = 0;
        virtual void agregarContacto() = 0;
        virtual void cerrarGuasap(FechaHora ultConexion) = 0;

};


#endif //GUASAP_IUSUARIOCONTROLLER_H
