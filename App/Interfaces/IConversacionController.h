//
// Created by sebiitta on 10/06/18.
//

#ifndef GUASAP_ICONVERSACIONCONTROLLER_H
#define GUASAP_ICONVERSACIONCONTROLLER_H

#include "../Tipos.h"
#include "../DtConversacion.h"
#include "../DtContacto.h"
#include "../DtInfoConversacion.h"
#include "../DtMensaje.h"
#include "../DtInfoMensajeEnviado.h"


class IConversacionController {

    public:
        virtual void archivarConversacion(int id) = 0;
        virtual void enviarMensajeContacto(string celular)= 0;
        virtual void enviarMensajeImagen(string url, FormatoImg formato, int tam, string desc) = 0;
        virtual void enviarMensajeSimple(string text) = 0;
        virtual void enviarMensajeVideo(string url, int duracion) = 0;
        virtual list<DtContacto *> listarContactos() = 0;
        virtual list<DtContacto *> listarContactosSeleccionados() = 0;
        virtual list<DtContacto *> listarContactosRestantes() = 0;
        virtual void agregarUsuario(string numTelefono) = 0; //agregar a la coleccion que de contactos seleccionados
        virtual void quitarUsuario(string numTelefono) = 0; //quitar de la coleccion de contactos seleccionados
        virtual void crearGrupo(string nombre, string imagen,FechaHora fhCreacion) = 0; //crea el grupo y para cada elementos de la coleccion de contactos seleccionados se crea un usuarioGrupo que se agrega a la coleccion de usuarioGrupo
        virtual void cancelar() = 0;
        virtual list<DtConversacion *> listarConversacionesActivas() = 0;
        virtual list<DtConversacion *> listarConversacionesArchivadas() = 0;
        virtual DtInfoConversacion& listarConversaciones() = 0;
        virtual void seleccionarContacto(string numCelular) = 0;
        virtual list<DtMensaje *> seleccionarConversacion(int idConv) = 0;
        virtual void seleccionarConversacionEnviar(int idConv) = 0;
        virtual void seleccionarMensajeParaEliminar(string codigo) = 0;
        virtual list<DtInfoMensajeEnviado *> verInfoMensajeEnviado(string codigo) = 0;

};


#endif //GUASAP_ICONVERSACIONCONTROLLER_H
