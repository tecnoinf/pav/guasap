//
// Created by seba on 10/06/18.
//

#include "Mensaje.h"

// Constructores
Mensaje::Mensaje() {
    this->codigo = "";
    this->fhEnviado = FechaHora();
    this->conversacion = nullptr;
    this->usuario = nullptr;
}

Mensaje::Mensaje(string codigo, FechaHora fhEnviado) {
    this->codigo = codigo;
    this->fhEnviado = fhEnviado;
    this->conversacion = nullptr;
    this->usuario = nullptr;
}

// Operaciones definidas en DCD
void Mensaje::desasignarUMensaje(UsuarioMensaje* um) {

    // Diagrama de comunicacion "seleccionarMensajeParaEliminaar".
    this->usuarioMensaje.erase(um->getReceptor()->getNumTelefono());

}

list<DtInfoMensajeEnviado *> Mensaje::getDatos() {

    // Diagrama de comunicacion "verInfoMensajeEnviado"
    list<DtInfoMensajeEnviado *> ret;

    for (auto &it : this->usuarioMensaje) {
        // Agregamos el puntero de lo recibido la lista.
        ret.push_back(&it.second->getDatos());
    }

    return ret;
}

// Getters
string Mensaje::getCodigo() {
    return this->codigo;
}

FechaHora Mensaje::getFhEnviado() {
    return this->fhEnviado;
}

Conversacion* Mensaje::getConversacion() {
    return this->conversacion;
}

Usuario* Mensaje::getUsuario() {
    return this->usuario;
}

UsuarioMensaje* Mensaje::getUsuarioMensaje(string clave) {
    return this->usuarioMensaje[clave];
}

// Setters
void Mensaje::setCodigo(string codigo) {
    this->codigo = codigo;
}

void Mensaje::setFhEnviado(FechaHora fhEnviado) {
    this->fhEnviado = fhEnviado;
}

void Mensaje::setConversacion(Conversacion *conversacion) {
    this->conversacion = conversacion;
}

void Mensaje::setUsuario(Usuario *usuario) {
    this->usuario = usuario;
}

void Mensaje::setUsuarioMensaje(string clave, UsuarioMensaje *usuarioMensaje) {
    this->usuarioMensaje[clave] = usuarioMensaje;
}

// Destructor
Mensaje::~Mensaje() {
    // Diagrama de comunicacion "seleccionarMensajeParaEliminar"

    for (auto &it : this->usuarioMensaje) {
        delete it.second;
    }
}
