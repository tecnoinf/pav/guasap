#ifndef DTCONTACTO_H
#define DTCONTACTO_H

#include <string>

using std::string;

class DtContacto{

    private:
        string numTelefono;
        string nombre;

    public:
        //Constructor
        DtContacto();
        DtContacto(string numTelefono, string nombre);

        //Getters
        string getNumTelefono();
        string getNombre();

};

std::ostream& operator <<(std::ostream& out, DtContacto& dtC);

#endif // DTCONTACTO_H
