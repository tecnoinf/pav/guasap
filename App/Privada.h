#ifndef GUASAP_PRIVADA_H
#define GUASAP_PRIVADA_H
#include "Conversacion.h"
#include "Usuario.h"
#include <string>


class Privada: public Conversacion {

private:
    Usuario *usuario;

public:

    //Constructores
    Privada();

    Privada(int id, Usuario *usuario);

    //Getters
    Usuario *getUsuario();

    //Setters
    void setUsuario(Usuario *usuario);

    //Operaciones
    DtConversacion* getDatos();

    list<DtMensaje *> getDatosMensaje(Usuario *us, string tel);

    //Destructor
    ~Privada();
};

#endif //GUASAP_PRIVADA_H
