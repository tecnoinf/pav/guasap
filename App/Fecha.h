//
// Created by seba on 09/06/18.
//

#ifndef GUASAP_DTFECHA_H
#define GUASAP_DTFECHA_H

class Fecha{

    private:
        int dia, mes, anio;

    public:
        Fecha();
        Fecha(int anio, int mes, int dia);

        int getDia();
        int getMes();
        int getAnio();
};

#endif //GUASAP_DTFECHA_H
