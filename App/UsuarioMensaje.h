#ifndef USUARIOMENSAJE_H_INCLUDED
#define USUARIOMENSAJE_H_INCLUDED


#include "FechaHora.h"
#include "DtInfoMensajeEnviado.h"
#include "Usuario.h"

class Mensaje;
class Usuario;

class UsuarioMensaje {

    private:
        bool visto;
        bool eliminado;
        FechaHora fhVisto;
        Usuario* receptor;
        Mensaje* msj;

    public:
        //Constructores
        UsuarioMensaje();
        UsuarioMensaje(bool visto, bool eliminado, FechaHora fhVisto);

        //Getters
        bool getVisto();
        bool getEliminado();
        FechaHora getfhVisto();
        DtInfoMensajeEnviado& getDatos();
        Usuario* getReceptor();
        Mensaje* getMsj();

        //Setters
        void setVisto(bool visto);
        void setEliminado(bool eliminado);
        void setFhVisto(FechaHora fhVisto);

        //Destructor
        ~UsuarioMensaje();
};



#endif // USUARIO-MENSAJE_H_INCLUDED
