
#ifndef GUASAP_CONVERSACION_H
#define GUASAP_CONVERSACION_H
#include <map>
#include <list>
#include <string>
#include "UsuarioConversacion.h"
#include "Mensaje.h"
#include "Usuario.h"
#include "DtConversacion.h"
#include "DtMensaje.h"

using std::string;
using namespace std;

class UsuarioConversacion;

class Usuario;

class Conversacion{

    private:
        int id;
        map<string, UsuarioConversacion *> colUsCon;

    protected:
        map<string, Mensaje *> colMensajes;

    public:
        //Constructores
        Conversacion();
        Conversacion(int id/*, map <string,UsuarioConversacion*> colUsCon, map<string,Mensaje*> colMensajes*/);

        //Getters
        int getId();
        UsuarioConversacion* getUsCon(string numTelefono);
        Mensaje* getMensaje(string codigo);
        map<string, Mensaje *>& getColMensajes();

        //Setters
        void setId(int id);
        void setColUsCon(string clave,UsuarioConversacion* usCon);
        void setColMensajes(string clave, Mensaje* mensaje);

        //Operaciones
        void asignarConversacion(UsuarioConversacion* uem);
        void enviarMensaje(Mensaje* msj);
        void enviarMensaje(Mensaje* msj, UsuarioConversacion* uem, UsuarioConversacion* ure);
        void desasignarMensaje(Mensaje* msj);
        virtual DtConversacion* getDatos() = 0;
        virtual list <DtMensaje *> getDatosMensaje(Usuario* us,string tel) = 0;
        void setActiva(bool activa);

        //Destructor
        ~Conversacion();
};

#endif //GUASAP_CONVERSACION_H
