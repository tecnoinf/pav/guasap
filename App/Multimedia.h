#ifndef GUASAP_MULTIMEDIA_H
#define GUASAP_MULTIMEDIA_H

#include "Mensaje.h"
#include "Usuario.h"
#include "DtMensaje.h"


class Multimedia: public Mensaje{

    public:
        //Constructores
        Multimedia();
        Multimedia(string codigo, FechaHora fhEnviado);

        //Operaciones
        virtual DtMensaje& getDatos(string tel,Usuario* us) = 0;

        //Destructor
        ~Multimedia();
};


#endif //GUASAP_MULTIMEDIA_H
