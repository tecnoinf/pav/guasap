
#ifndef GUASAP_GRUPAL_H
#define GUASAP_GRUPAL_H
#include "Conversacion.h"
#include "Grupo.h"
#include "Usuario.h"
#include <string>


class Grupal :public Conversacion{

private:
    Grupo* grupo;

public:

    //Constructores
    Grupal();
    Grupal(int id, Grupo* grupo);

    //Getters
    Grupo* getGrupo();
    //Setters
    void setGrupo(Grupo* grupo);

    //Operaciones
    DtConversacion* getDatos();
    list <DtMensaje *> getDatosMensaje(Usuario* us,string tel);
    //Destructor
    ~Grupal();

};




#endif //GUASAP_GRUPAL_H
