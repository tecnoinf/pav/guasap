#include "DtConversacion.h"

//Constructores
DtConversacion::DtConversacion(){
    this->id = 0;
}

DtConversacion::DtConversacion(int id) {
    this->id = id;
}

//Getters
int DtConversacion::getId(){
    return id;
}


// Destructor
DtConversacion::~DtConversacion() = default;

std::ostream& operator <<(std::ostream& out, DtConversacion& dtc) {
    return out << dtc.getId();
}