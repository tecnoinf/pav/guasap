
#include "DtConvGrupo.h"
#include "DtConversacion.h"
#include <string>


using std::string;

DtConvGrupo::DtConvGrupo() :DtConversacion(){
    this->nomGrupo = "";
}

DtConvGrupo::DtConvGrupo(int id, string nomGrupo) :DtConversacion(id){
    this->nomGrupo = nomGrupo;
}

string DtConvGrupo::getNomGrupo() {
    return nomGrupo;
}
