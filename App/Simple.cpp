//
// Created by sebiitta on 10/06/18.
//

#include "Simple.h"
#include "Mensaje.h"
#include "DtMsjSimple.h"
#include <string>

using namespace std;

Simple::Simple(): Mensaje()
{
    this->texto="";
}

Simple::Simple(string codigo, FechaHora fhEnviado, string texto): Mensaje(codigo, fhEnviado){

    this->texto = texto;
};

DtMensaje& Simple::getDatos(string tel, Usuario* us)
{
    auto dtMsSimple = new DtMsjSimple(getTexto(), getCodigo(), getFhEnviado());
    return *dtMsSimple;
}

string Simple::getTexto() {
    return this->texto;
}

Simple::~Simple()=default;
