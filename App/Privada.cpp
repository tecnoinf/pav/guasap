#include "Privada.h"
#include "Conversacion.h"
#include "Usuario.h"
#include "DtConvPrivada.h"
#include "DtConversacion.h"
#include "DtMensaje.h"
#include <string>
#include <list>
#include "Imagen.h"
#include "Simple.h"
#include "Video.h"
#include "Contacto.h"

//Constructores
Privada::Privada(): Conversacion() {
    this->usuario = nullptr;
}

Privada::Privada(int id, Usuario* usuario):Conversacion(id) {
    this->usuario = usuario;
}

//Getters

Usuario* Privada::getUsuario(){
    return usuario;
}

//Setters

void Privada::setUsuario(Usuario* usuario){
    this->usuario = usuario;
}

//Operaciones

DtConversacion* Privada::getDatos() {


    Usuario* usuario = getUsuario();
    string nombre = usuario->getNombre();
    string numTelefono = usuario->getNumTelefono();
    DtConvPrivada* dtConv = new DtConvPrivada(getId(),numTelefono, nombre);

    return dtConv;


}

list <DtMensaje *> Privada::getDatosMensaje(Usuario* us, string tel){

    list <DtMensaje *> listDtMensajes;

    for (auto &it : this->colMensajes) {
        DtMensaje* dtm;
        auto aux = it.second;
        
        
        auto asd1 = dynamic_cast<Imagen *>(aux);
        auto asd2 = dynamic_cast<Simple *>(aux);
        auto asd3 = dynamic_cast<Video *>(aux);
        auto asd4 = dynamic_cast<Contacto *>(aux);

        if (asd1 != nullptr) {
            dtm = &asd1->getDatos(tel, us);
        }
        else if (asd2 != nullptr) {
            dtm = &asd2->getDatos(tel, us);
        }
        else if (asd3 != nullptr) {
            dtm = &asd3->getDatos(tel, us);
        }
        else {
            dtm = &asd4->getDatos(tel, us);
        }

        //auto dtm = it.second->getDatos(tel, us);
        //auto dtm = asd->getDatos();
        listDtMensajes.push_back(dtm);
    }
    return listDtMensajes;

}


//Destructor

Privada::~Privada() {
    //delete usuario;
}

