//
// Created by sebiitta on 09/06/18.
//

#ifndef GUASAP_DTMSJSIMPLE_H
#define GUASAP_DTMSJSIMPLE_H


#include "DtMensaje.h"

class DtMsjSimple: public DtMensaje {
    private:
        string texto;

    public:
        DtMsjSimple();
        DtMsjSimple(string texto, string codigo, FechaHora fhEnviado);

        string getTexto();
};


#endif //GUASAP_DTMSJSIMPLE_H
