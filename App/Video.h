#ifndef VIDEO_H_INCLUDED
#define VIDEO_H_INCLUDED


#include "Multimedia.h"
#include <string>
#include "DtMensaje.h"

class Video: public Multimedia{

    private:
        int duracion;
        string url;

    public:

        //Constructores
        Video();
        Video(string codigo, FechaHora fhEnviado,int duracion, string url);

        //Getters
        int getDuracion();
        string getUrl();
        DtMensaje& getDatos(string tel, Usuario *us);

        //Setters
        void setDuracion(int duracion);
        void setUrl(string url);

        //Destructor
        ~Video();

};
#endif // VIDEO_H_INCLUDED
