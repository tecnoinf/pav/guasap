#include "Conversacion.h"
#include <map>
#include <list>
#include <string>
#include "UsuarioConversacion.h"
#include "Mensaje.h"
#include "Usuario.h"
#include "DtConversacion.h"
#include "DtMensaje.h"

using namespace std;


//Constructores

Conversacion::Conversacion() {
    this->id = 0;
}

Conversacion::Conversacion(int id/*, map<string, UsuarioConversacion*> colUsCon, map<string, Mensaje*> colMensajes*/) {
    this->id = id;
    //this->colUsCon = colUsCon;
    //this->colMensajes = colMensajes;
}

//Getters

int Conversacion::getId(){
    return id;
}

UsuarioConversacion* Conversacion::getUsCon(string numTelefono){
    return colUsCon[numTelefono];
}

Mensaje* Conversacion::getMensaje(string codigo){
    return colMensajes[codigo];
}

map<string, Mensaje *>& Conversacion::getColMensajes() {
    return this->colMensajes;
}

//Setters

void Conversacion::setId(int id){
    this->id = id;
}
void Conversacion::setColUsCon(string clave,UsuarioConversacion* usCon){
    this->colUsCon[clave] = usCon;
}
void Conversacion::setColMensajes(string clave, Mensaje* mensaje){
    this->colMensajes[clave] = mensaje;
}

//Operaciones

void Conversacion::asignarConversacion(UsuarioConversacion* uem){

    Usuario* usuario = uem->getUsuario(); //conseguir el usuario relacionado a un usuario conversacioj (supongo que va a tener un get al usuario)
    string clave = usuario->getNumTelefono();
    setColUsCon(clave, uem); // lo agrego a la coleccion de usuario conversacion
}

void Conversacion::enviarMensaje(Mensaje* msj) {

    //agregar el mensaje a la coleccion personal de la conversacion
    string clave = msj->getCodigo();
    setColMensajes(clave, msj);
}

void Conversacion::enviarMensaje(Mensaje* msj, UsuarioConversacion* uem, UsuarioConversacion* ure) {

    this->enviarMensaje(msj);

    // Establecemos como activa la conversacion para los dos usuarios.
    uem->setEstaActiva(true);
    ure->setEstaActiva(true);
}

void Conversacion::desasignarMensaje(Mensaje* msj){

    colMensajes.erase(msj->getCodigo());
}

void Conversacion::setActiva(bool activa){

    for (auto &it : colUsCon)
        it.second->setEstaActiva(true);
}

//Destructor
Conversacion::~Conversacion() {

    for (auto &it : colUsCon)
        colUsCon.erase(it.second->getUsuario()->getNumTelefono());

    for (auto &it : colMensajes)
        desasignarMensaje(it.second);

}
