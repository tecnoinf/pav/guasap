
#ifndef GUASAP_DTCONVERSACION_H
#define GUASAP_DTCONVERSACION_H

#include <iostream>

class DtConversacion{

private:
    int id;

public:
    //Constructores
    DtConversacion();
    DtConversacion(int id);

    //Getters
    int getId();

    // Destructor
    virtual ~DtConversacion();
};

std::ostream& operator <<(std::ostream& out, DtConversacion& dtc);

#endif //GUASAP_DTCONVERSACION_H
