
#include "DtInfoContacto.h"
#include "DtContacto.h"
#include <string>
#include <iostream>

using std::string;

//Constructores

DtInfoContacto::DtInfoContacto() :DtContacto(){
    this->imgPerfil = "";
}

DtInfoContacto::DtInfoContacto(string numTelefono,string nombre, string imgPerfil) :DtContacto(numTelefono,nombre){
    this->imgPerfil = imgPerfil;
}

string DtInfoContacto::getImgPerfil(){
    return imgPerfil;
}

std::ostream& operator <<(std::ostream& out, DtInfoContacto& dtIC) {
    out << "Número de Teléfono: " << dtIC.getNumTelefono() << ", Nombre: " << dtIC.getNombre()
        << ", Imagen de Perfil: " << dtIC.getImgPerfil() << std::endl;

    return out;
}
