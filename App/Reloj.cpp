//
// Created by seba on 23/06/18.
//

#include "Reloj.h"
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <string>

using namespace std;

// Instancia del Reloj.
Reloj* Reloj::instance = nullptr;

// Constructores.
Reloj::Reloj()
{
    this->usarHoraSistema=true;
    this->hora=FechaHora();
}

Reloj::Reloj(bool usarHoraSistema, FechaHora hora)
{
    this->usarHoraSistema=usarHoraSistema;
    this->hora=hora;
}

// Retornamos la instancia existente, o una nueva.
Reloj* Reloj::getInstance()
{
    if (instance == nullptr) {
        instance = new Reloj();
    }

    return instance;
}

// Operacion que retorna la hora del sistema operativo.
// En mi opinion deberia ser privada...
FechaHora Reloj::getHoraSistema()
{
    unsigned int micros;

    micros=1000000;
    usleep(micros);
    time_t t = time(nullptr);
    struct tm tm = *localtime(&t);
    return FechaHora((tm.tm_year + 1900), (tm.tm_mon + 1), (tm.tm_mday), (tm.tm_hour), (tm.tm_min),(tm.tm_sec));

}

FechaHora Reloj::dimeLaHora()
{
    if (usarHoraSistema) {
        return getHoraSistema();
    }
    else {
        return this->hora;
    }
}

void Reloj::setHora(FechaHora hora)
{
    this->hora=hora;
}

void Reloj::setUsarHoraSistema(bool HoraSis)
{
    this->usarHoraSistema=HoraSis;
}

bool Reloj::getUsarHoraSistema()
{
    return usarHoraSistema;
}

// Destructor
Reloj::~Reloj()
{
    instance = nullptr;
}
