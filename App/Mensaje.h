//
// Created by seba on 10/06/18.
//

#ifndef GUASAP_MENSAJE_H
#define GUASAP_MENSAJE_H

#include <string>
#include "FechaHora.h"
#include "Conversacion.h"
#include "Usuario.h"
#include "UsuarioMensaje.h"
#include "DtMensaje.h"
#include "DtInfoMensajeEnviado.h"
#include <list>
#include <map>

using namespace std;

class Conversacion;

class UsuarioMensaje;

class Mensaje {

    private:
        string codigo;
        FechaHora fhEnviado;
        Conversacion* conversacion; // TODO: Verificar si es necesario el pseudoatributo de la conversacion.
        Usuario* usuario;
        map<string, UsuarioMensaje *> usuarioMensaje;

    public:
        // Constructores
        Mensaje();
        Mensaje(string codigo, FechaHora fhEnviado);

        // Operaciones definidas en DCD
        void desasignarUMensaje(UsuarioMensaje* um);
        virtual DtMensaje& getDatos(string tel, Usuario *us) = 0; // TODO: Revisar el orden de los parametros.
        list<DtInfoMensajeEnviado *> getDatos();

        // Getters
        string getCodigo();
        FechaHora getFhEnviado();
        Conversacion* getConversacion();
        Usuario* getUsuario();
        UsuarioMensaje* getUsuarioMensaje(string clave);

        // Setters
        void setCodigo(string codigo);
        void setFhEnviado(FechaHora fhEnviado);
        void setConversacion(Conversacion* conversacion);
        void setUsuario(Usuario* usuario);
        void setUsuarioMensaje(string clave, UsuarioMensaje* usuarioMensaje);

        // Destructor
        virtual ~Mensaje();
};


#endif //GUASAP_MENSAJE_H
