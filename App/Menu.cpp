#include <iostream>
#include <string>
#include <sstream>
#include "Menu.h"
#include "DtInfoContacto.h"
#include "FechaHora.h"
#include "DtConvPrivada.h"
#include "DtConvGrupo.h"
#include "Conversacion.h"
#include "Privada.h"
#include "Grupal.h"
#include <typeinfo>



#define clearscr() printf("\033[H\033[J");

// Instancia del Menu.
Menu* Menu::instance = nullptr;

// Constructor
Menu::Menu() {
    // Obtenemos las instancias de las fabricas.
    this->fuc = FUsuarioController::getInstance();
    this->fcc = FConversacionController::getInstancia();

    // Obtenemos las instancias de las interfases.
    this->iuc = this->fuc->getIUsuarioController();
    this->icc = this->fcc->getIConversacionController();

    // Obtenemos la instancia del reloj.
    this->rlj = Reloj::getInstance();
    this->loggedIn = false;
}

Menu* Menu::getInstance() {
    if (instance == nullptr) {
        instance = new Menu();
    }

    return instance;
}

void Menu::printMenu() {
    cout << endl << "    Bienvenido a Guasap!" << endl << endl;

    cout << " Menu principal" << endl;
    cout << "    1- Abrir Guasap" << endl;
    cout << "    2- Cerrar Guasap" << endl;
    cout << "    3- Agregar contactos" << endl;
    cout << "    4- Alta grupo" << endl;
    cout << "    5- Enviar mensaje" << endl;
    cout << "    6- Ver mensaje" << endl;
    cout << "    7- Archivar conversacion" << endl;
    cout << "    8- Modificar usuario" << endl;
    cout << "    9- Eliminar mensajes" << endl;
    cout << "   10- Configurar Fecha y Hora" << endl;
    cout << "   11- Cargar datos" << endl;
    cout << "   12- Salir" << endl;

    cout << endl << "  Elija una opcion: ";
}

void Menu::mainMenu() {
    string opt;
    //bool loggedIn = false;

    do {
        // Se limpia la pantalla.
        //clearscr();

        // Imprimimos el menu
        this->printMenu();

        // Leemos la opcion del usuario.
        cin >> opt;
        cin.ignore();

        try {
            // Definimos llamamos a los handlers en base a la eleccion del usuario.
            if (opt == "1") {
                this->handleOption1();

            } else if (opt == "2") {
                this->handleOption2();

            } else if (opt == "3") {
                if (getLoggedIn()) {
                    this->handleOption3();
                } else {
                    cout << "Para agregar contactos, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "4") {
                if (getLoggedIn()) {
                    this->handleOption4();
                } else {
                    cout << "Para dar de alta un grupo, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "5") {
                if (getLoggedIn()) {
                    this->handleOption5();
                } else {
                    cout << "Para enviar mensajes, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "6") {
                if (getLoggedIn()) {
                    this->handleOption6();
                } else {
                    cout << "Para ver mensajes, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "7") {
                if (getLoggedIn()) {
                    this->handleOption7();
                } else {
                    cout << "Para archivar conversaciones, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "8") {
                if (getLoggedIn()) {
                    this->handleOption8();
                } else {
                    cout << "Para modificar usuarios, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "9") {
                if (getLoggedIn()) {
                    this->handleOption9();
                } else {
                    cout << "Para eliminar mensajes, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "10") {
                if (getLoggedIn()) {
                    this->handleOption10();
                } else {
                    cout << "Para configurar la hora, debe inciar sesión." << endl;
                    cin.ignore();
                }
            } else if (opt == "11") {
                this->handleOption11();
            }

            else if (opt == "12") {
                this->handleOption12();
            }

        } catch (invalid_argument &e) {
            this->handleExceptions(e.what());
        }

    } while (opt != "12");
}

//Getter & setter
bool Menu::getLoggedIn(){
    return this->loggedIn;
}
void Menu::setLoggedIn(bool log){
    this->loggedIn = log;
}

void Menu::handleOption1() {

    string cel, opt, nombre, urlFoto, descripcion;
    bool existe = false, cancelar = false;

    while ( ! existe && ! cancelar) {
        cout << endl << "Ingrese su numero de celular: ";
        getline(cin, cel);

        existe = this->iuc->ingresarCelular(cel);
        setLoggedIn(existe);
        if ( ! existe) {
            cout << "El usuario no existe. ¿Desea darse de alta?" << endl;
            cout << "Ingrese \"S\" para darse de alta, o \"N\" para cancelar: ";

            do {
                cin >> opt;
                cin.ignore();

                if (opt == "S" || opt == "s") {
                    cout << "Ingrese su nombre: ";
                    getline(cin, nombre, '\n');

                    cout << "Ingrese la URL de su foto de perfil: ";
                    getline(cin, urlFoto, '\n');

                    cout << "Ingrese una descripcion: ";
                    getline(cin, descripcion, '\n');

                    // Ingresamos los datos para crear el usuario.
                    this->iuc->ingresarDatos(nombre, urlFoto, descripcion);

                    // Cortamos el loop.
                    existe = true;
                    setLoggedIn(true);

                } else if (opt == "N" || opt == "n") {
                    // Cancelamos el alta del usuario.
                    this->iuc->cancelar();

                    // Cortamos el loop.
                    cancelar = true;

                } else {
                    cout << "Opcion desconocida, ingrese nuevamente: ";
                }

            } while (opt != "S" && opt != "s" && opt != "N" && opt != "n");
        }
    }
}

void Menu::handleOption2() {
    string opt;
    cout << "¿Está seguro que desea cerrar Guassap?" << endl;
    cout << "Ingrese \"S\" para cerrar Guassap "<< endl;
    cin >> opt;
    cin.ignore();
    if (opt == "S" || opt == "s") {
        this->iuc->cerrarGuasap(this->rlj->dimeLaHora());
        setLoggedIn(false);
    }
}

void Menu::handleOption3() {
    string numTelefono,opt;
    //Agregar contacto
    do {

        cout<<"Contactos agregados: "<<endl;
        for (auto &it : this->iuc->listarContactosDeUsuario())
            cout << it;
        try {
            cout << endl << "Ingrese el número de Teléfono del contacto que se desea agregar: " << endl;
            cin >> numTelefono;
            cin.ignore();
            cout << this->iuc->indicarNumero(numTelefono) << endl;
            cout << "Presione \"S\" para confirmar : " << endl;
            cin >> opt;
            cin.ignore();
            if (opt == "S" || opt == "s") {
                this->iuc->agregarContacto();
            }
            cout << "Digite \"S\" si desea agregar mas contactos " << endl;
            cin >> opt;
            cin.ignore();

        }
        catch (std::invalid_argument &e)
        {
            handleExceptions(e.what());
        }
    }while ((opt == "S" || opt == "s") && (opt != "N" && opt != "n"));
}



void Menu::handleOption4() {
    string numTelefono,opt,nombre,imagen;

    //Alta grupo
    do {


        try {
            cout<< "Contactos Seleccionados:"<<endl;
            for (auto &it : this->icc->listarContactosSeleccionados())
                cout << *it;

            cout<<"Contactos Restantes: " <<endl;
            for (auto &it : this->icc->listarContactosRestantes())
                cout << *it;

            cout << "Ingrese: " << endl << "\"A\" para seleccionar un contacto." << endl << "\"Q\" para deseleccionar un contacto."<< endl<<"\"S\" para crear el Grupo con los contactos elegidos."<< endl << "\"C\" para Cancelar: " << endl;
            cin >> opt;
            if (opt == "A" || opt == "a") {

                cout<<"Ingrese el numero de telefono del contacto que desea agregar al grupo: "<<endl;
                cin>>numTelefono;
                this->icc->agregarUsuario(numTelefono);
            }

            if (opt == "Q" || opt == "q") {

                cout<<"Ingrese el numero de telefono del contacto que desea quitar del grupo: "<<endl;
                cin>>numTelefono;
                this->icc->quitarUsuario(numTelefono);
            }

            if (opt == "S" || opt == "s") {
                cout<<"Ingrese el nombre del grupo: "<<endl;
                cin>>nombre;
                cout<<"Ingrese el url de la imagen del grupo: "<<endl;
                cin>>imagen;
                this->icc->crearGrupo(nombre,imagen,rlj->dimeLaHora());
                cout<<"Grupo creado correctamente."<<endl;
            }

            if (opt == "C" || opt == "c") {
                this->icc->cancelar();
            }

        }
        catch (std::invalid_argument &e)
        {
            handleExceptions(e.what());
        }
    }while (opt != "S" && opt != "s" && opt != "C" && opt != "c");

}

void Menu::handleOption5() {
    string opt;

    DtInfoConversacion& dtIC = icc->listarConversaciones();
    cout << dtIC;

    cout << "*** Opciones ***" << endl;
    cout << "   1- Seleccionar conversacion activa" << endl;
    cout << "   2- Ver las conversaciones archivadas" << endl;
    cout << "   3- Enviar mensaje a un contacto nuevo" << endl;
    cout << endl << "Elija una opcion: ";
    cin >> opt;
    cin.ignore();

    if (opt == "3") {
        string numCelular;
        list<DtContacto *> contactos = this->icc->listarContactos();

        if (contactos.empty()) {
            throw invalid_argument("No tienes contactos");
        }

        for (auto it : contactos) {
            cout << *it;
        }

        cout << "Indique el numero del contacto: ";
        cin >> numCelular;
        cin.ignore();

        this->icc->seleccionarContacto(numCelular);

    } else {
        string idConv;

        if (opt == "2") {
            // Puede ser que el ampersand no vaya en la declaracion de it.
            for (auto &it : this->icc->listarConversacionesArchivadas()) {
                cout << it;
            }
        }

        cout << "Indique el ID de la conversacion: ";
        cin >> idConv;
        cin.ignore();

        try {
            // La funcion stoi() lanza una excepcion del tipo std::invalid_argument.
            this->icc->seleccionarConversacionEnviar(stoi(idConv));

        } catch (std::invalid_argument &e) {
            this->handleExceptions(e.what());
        }
    }

    cout << "*** Tipos de mensaje ***" << endl;
    cout << "   1- Mensaje Simple" << endl;
    cout << "   2- Mensaje Imagen" << endl;
    cout << "   3- Mensaje Video" << endl;
    cout << "   4- Mensaje Contacto" << endl;
    cout << endl << "Elija una opcion: ";
    cin >> opt;
    cin.ignore();

    if (opt == "1") {
        string texto;

        cout << "Ingrese el texto del mensaje: ";
        getline(cin, texto);

        this->icc->enviarMensajeSimple(texto);

    } else if (opt == "2") {
        string url, tmpFormat, tam, desc;
        FormatoImg formato;

        cout << "Ingrese la URL de la imagen: ";
        getline(cin, url);

        cout << "Ingrese el formato de la imagen [jpg/png/gif]: ";
        getline(cin, tmpFormat);

        // TODO: Cambiar esto, si no se ingresa un formato existente se debe pedir nuevamente.
        if (tmpFormat == "jpg") { formato = jpg; }
        else if (tmpFormat == "png") { formato = png; }
        else { formato = gif; }

        cout << "Ingrese el tamaño de la imagen: ";
        getline(cin, tam);

        cout << "Ingrese la descripcion de la imagen: ";
        getline(cin, desc);

        // TODO: Manejar excepcion del stoi().
        this->icc->enviarMensajeImagen(url, formato, stoi(tam), desc);
    }
    else if (opt == "3") {
        string url, duracion;

        cout << "Ingrese la URL del video: ";
        getline(cin, url);

        cout << "Ingrese la duracion del video: ";
        getline(cin, url);

        // TODO: Manejar excepcion del stoi().
        this->icc->enviarMensajeVideo(url, stoi(duracion));
    }
    else if (opt == "4") {
        string numCelular;

        for (auto &it : this->icc->listarContactos()) {
            cout << *it;
        }

        cout << "Indique el numero del contacto: ";
        cin >> numCelular;

        this->icc->enviarMensajeContacto(numCelular);
    }
    else {
        cout << "Opcion desconocida!" << endl;
    }
}

void Menu::handleOption6() {

}

void Menu::handleOption7() {

    int numConv = 0;
    string opt;


    list <DtConversacion *> listaConver = this->icc->listarConversacionesActivas();

    if(!listaConver.empty()){

        for (auto &it : listaConver) {

            auto ConvPriv = dynamic_cast<DtConvPrivada *> (it);
            if (ConvPriv != nullptr) {
                DtConvPrivada *dtp = ConvPriv;
                cout << dtp->getId() << "  -  " << dtp->getNumTelefono() << "  -  " << dtp->getNombre() << endl;
                continue;
            }

            auto ConvGrup = dynamic_cast<DtConvGrupo *> (it);
            if (ConvGrup != nullptr) {
                DtConvGrupo *dtg = ConvGrup;
                cout << dtg->getId() << "  -  " << dtg->getNomGrupo() << endl;
                continue;
            }
        }

        do {
            cout << "Quiere archivar conversaciones?" << endl << endl;;
            cout << "Presione \"S\" o \"s\" para archivar otra conversacion" << endl;
            cin >> opt;
            //cin.ignore();

            if((opt == "S") || (opt == "s")){
                cout << "Ingrese el numero de la conversacion que desea archivar: ";
                cin >> numConv;
                this->icc->archivarConversacion(numConv);
            }

        }while(opt == "S" || opt == "s");
    }else{
        cout << "No hay conversaciones" << endl;
    }

}
//Seba O
void Menu::handleOption8() {
    string opt;
    string nuevoNombre, nuevaImgPerfil, nuevaDescripcion;
    clearscr();
    cout << "     Modificación de Usuarios\n" << endl;
    do {
        cout << " Ingrese el nuevo nombre de usuario, luego presione Enter:";
        getline(cin, nuevoNombre);
        cout << "Para Guardar los cambios presione\"G\", para Cancelar \"C\" ,para Salir \"S\" luego presione enter:";
        cin >> opt;
        cin.ignore();
        if (opt == "G" || opt == "g") { this->iuc->ingresarDatosNombre(nuevoNombre); }
        else if(opt == "s" || opt == "S"){continue; }

        cout << "ingrese la URL de la nueva imagen de perfil, luego presione Enter:";
        getline(cin, nuevaImgPerfil);
        cout << "Para Guardar los cambios presione\"G\", para Cancelar \"C\",para Salir \"S\" luego presione enter:";
        cin >> opt;
        cin.ignore();
        if (opt == "G" || opt == "g") { this->iuc->ingresarDatosUFoto(nuevaImgPerfil);}
        else if(opt == "s" || opt == "S"){continue; }

        cout << "ingrese la nueva descripción de usuario, luego presione Enter:";
        getline(cin, nuevaDescripcion);
        cout << "Para Guardar los cambios presione\"G\", para Cancelar \"C\",para Salir \"S\"luego presione enter:";
        cin >> opt;
        cin.ignore();
        if (opt == "G" || opt == "g") { this->iuc->ingresarDatosDescripcion(nuevaDescripcion);}
        else if(opt == "s" || opt == "S"){continue; }

        opt = "s";
    }while (opt != "s" && opt != "S");

}

void Menu::handleOption9() {
    string opt, idConv, idMsj;

    DtInfoConversacion& dtIC = icc->listarConversaciones();
    cout << dtIC;

    if (dtIC.getConvActivas().empty()) {
        throw invalid_argument("El usuario no tiene conversaciones.");
    }

    cout << "*** Opciones ***" << endl;
    cout << "   1- Seleccionar conversacion activa" << endl;
    cout << "   2- Ver las conversaciones archivadas" << endl;
    cout << endl << "Elija una opcion: ";
    cin >> opt;
    cin.ignore();

    if (opt == "2") {
        // Puede ser que el ampersand no vaya en la declaracion de it.
        for (auto &it : this->icc->listarConversacionesArchivadas()) {
            cout << it;
        }
    }

    cout << "Indique el ID de la conversacion: ";
    cin >> idConv;
    cin.ignore();

    try {
        // La funcion stoi() lanza una excepcion del tipo std::invalid_argument.
        auto msjs = this->icc->seleccionarConversacion(stoi(idConv));
        for (auto it : msjs) {
            cout << *it;
        }

        cout << "Indique el codigo del mensaje que desea eliminar: ";
        cin >> idMsj;
        cin.ignore();

        // Se elimina el mensaje
        this->icc->seleccionarMensajeParaEliminar(idConv);

    } catch (std::invalid_argument &e) {
        this->handleExceptions(e.what());
    }
}

void Menu::handleOption10() {
    clearscr();
    string opt;

    do {

        if (rlj->getUsarHoraSistema()) {
            cout << endl;
            cout << "Se está utilizando Fecha y Hora del Sistema" << endl;
        } else {
            cout << " Se está utilizando Fecha y Hora ingresada por el usuario" << endl << endl;
        }
        cout << " La fecha actual es: " << rlj->dimeLaHora().getDia() << "/" << rlj->dimeLaHora().getMes() << "/"
             << rlj->dimeLaHora().getAnio() << endl;
        cout << " La hora actual es: " << rlj->dimeLaHora().getHora() << ":" << rlj->dimeLaHora().getMinuto() << ":"
             << rlj->dimeLaHora().getSegundo() << endl;
        cout << " Presione S para Utilizar la hora del Sistema" << endl;
        cout << " Presione M para ingresar hora manualmente" << endl;
        cout << " Presione X para salir" << endl;
        cout << " Opción:";
        cin >> opt;
        cin.ignore();

        if (opt == "M" || opt == "m") {

            string datos;
            cout << "Ingrese fecha DD/MM/AAAA: ";
            getline(cin, datos);
            stringstream ss;
            ss << datos;
            string sdia, smes, sanio, shora, sminuto, ssegundo;
            getline(ss, sdia, '/');
            getline(ss, smes, '/');
            getline(ss, sanio, ' ');
            //cin.ignore();

            cout << "Ingrese hora hh:mm:ss: ";
            string datos2;
            getline(cin, datos2);
            stringstream ss2;
            ss2 << datos2;
            getline(ss2, shora, ':');
            getline(ss2, sminuto, ':');
            getline(ss2, ssegundo, ' ');

            //cin.ignore();
            FechaHora horaUsuario(atoi(sanio.c_str()), atoi(smes.c_str()), atoi(sdia.c_str()), atoi(shora.c_str()),
                                  atoi(sminuto.c_str()), atoi(ssegundo.c_str()));
            rlj->setHora(horaUsuario);
            rlj->setUsarHoraSistema(false);

            opt="x";

        } else if (opt == "S" || opt == "s") {
            rlj->setUsarHoraSistema(true);
            cout << "Se utiliza hora del Sistema" << endl;
            cin.ignore();
        }
    }while( opt != "X" && opt != "x" && opt!= "S" && opt!="s" && opt!="N" && opt!="n");

}

void Menu::handleOption11() {
//Carga datos

    //*************************Usuarios*************************
    //U1 Juan Perez
    bool existe = this->iuc->ingresarCelular("090 12 36 54");
    this->iuc->ingresarDatos("Juan Pérez"," home/img/perfil/juan.png", "Amo usar Guasap");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());

    //U2 Maria Fernandéz
    existe = this->iuc->ingresarCelular("090 76 54 32");
    this->iuc->ingresarDatos("Maria Fernandéz"," home/img/perfil/maria.png", "Me encanta Avanzada");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());

    //U3 Pablo Iglesias
    existe = this->iuc->ingresarCelular("090 24 68 10");
    this->iuc->ingresarDatos("Pablo Iglesias"," home/img/perfil/pablo.png", "Me gustan los animales");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());

    //U4 Sara Ruiz
    existe = this->iuc->ingresarCelular("090 66 67 77");
    this->iuc->ingresarDatos("Sara Ruiz"," home/img/perfil/sara.png", "¡Estoy feliz!");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());

/*
    //***************************Contactos*************************

    //U1 Juan Perez
    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    auto dt = this->iuc->indicarNumero("090 76 54 32"); //U2
    this->iuc->agregarContacto(); //agrego U2
    dt = this->iuc->indicarNumero("090 24 68 10"); //U3
    this->iuc->agregarContacto(); //agrego U3
    dt = this->iuc->indicarNumero("090 66 67 77"); //U4
    this->iuc->agregarContacto(); //agrego U4
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora()); //cerro sesion U1

    //U2 --- U1,U3
    existe = this->iuc->ingresarCelular("090 76 54 32"); //Inicio sesion U2
    dt = this->iuc->indicarNumero("090 12 36 54"); //U1
    this->iuc->agregarContacto(); //agrego U1
    dt = this->iuc->indicarNumero("090 24 68 10"); //U3
    this->iuc->agregarContacto(); //agrego U3
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora()); //Cerro sesion U2

    //U3 --- U1,U2,U4
    existe = this->iuc->ingresarCelular("090 24 68 10"); //Inicio sesion U3
    dt = this->iuc->indicarNumero("090 12 36 54"); //U1
    this->iuc->agregarContacto(); //agrego U1
    dt = this->iuc->indicarNumero("090 76 54 32"); //U2
    this->iuc->agregarContacto(); //agrego U2
    dt = this->iuc->indicarNumero("090 66 67 77"); //U4
    this->iuc->agregarContacto(); //agrego U4
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora()); //cero sesion U3

    //U4 --- U1,U3
    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    dt = this->iuc->indicarNumero("090 12 36 54"); //U1
    this->iuc->agregarContacto(); //agrego U1
    dt = this->iuc->indicarNumero("090 24 68 10"); //U3
    this->iuc->agregarContacto(); //agrego U3
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora()); //cerro sesion U4

    //***************************Conversacion y Mensajes*************************

    //G1 Grupo Amigos U1,U2,U3,U4 home/img/amigos.png 22/05/2017 15:35 U1

    FechaHora fh = FechaHora(2017, 05, 22, 15, 35, 00);
    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->agregarUsuario("090 76 54 32"); //U2
    this->icc->agregarUsuario("090 24 68 10"); //U3
    this->icc->agregarUsuario("090 66 67 77"); //U4
    this->icc->crearGrupo("Amigos", "home/img/amigos.png", fh);
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    //M1 G1 U4 U1,U3 22/05/2017 18:04 Simple ¡Miren que bueno este video!
    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    this->icc->seleccionarConversacionEnviar(1); //no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo creado arriba
    this->icc->enviarMensajeSimple("Miren este video!");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U4

    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    existe = this->iuc->ingresarCelular("090 24 68 10"); //Inicio sesion U3
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U3

    //M2 G1 U4 U1,U3 22/05/2018 18:05 Video Duración: 05 min

    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    this->icc->seleccionarConversacionEnviar(1); //no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo creado arriba
    this->icc->enviarMensajeVideo("home/videos/gracioso.mp4",5);
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U4

    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    existe = this->iuc->ingresarCelular("090 24 68 10"); //Inicio sesion U3
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U3

    //M3 G1 U1 U3,U4 22/05/2018 18:12 Simple ¡Muy gracioso!

    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->seleccionarConversacionEnviar(1); //no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->enviarMensajeSimple("¡Muy gracioso!");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    existe = this->iuc->ingresarCelular("090 24 68 10"); //Inicio sesion U3
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U3

    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U4

    //M4 G1 U3 U1,U4 22/05/2018 18:13 Simple ¡Excelente!

    existe = this->iuc->ingresarCelular("090 24 68 10"); //Inicio sesion U3
    this->icc->seleccionarConversacionEnviar(1); //no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->enviarMensajeSimple("¡Excelente!");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U3

    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U4

    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->seleccionarConversacion(1);//no se como conseguir el id del grupo por que es al azar, necesito seleccionar el grupo
    this->icc->verInfoMensajeEnviado("codigo de mensaje que no se");//conseguir el mensaje que se envio antes.
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    //M5 CS2 U2 U1 23/05/2018 12:23 Simple Hola, me pasas el contacto de Sara que no lo tengo

    existe = this->iuc->ingresarCelular("090 76 54 32"); //Inicio sesion U2
    this->icc->seleccionarContacto("090 12 36 54"); //selecciono a U1
    this->icc->enviarMensajeSimple("Hola, me pasas el contacto de Sara que no lo tengo");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U2

    //M6 CS2 U1 U2 23/05/2018 12:25 Contacto Sara Ruiz, 090 66 67 77

    existe = this->iuc->ingresarCelular("090 12 36 54"); //Inicio sesion U1
    this->icc->seleccionarConversacionEnviar(2); //codigo de la conversacion privada recien creada
    this->icc->enviarMensajeContacto("090 66 67 77");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U1

    //M7 CS2 U2 U1 23/05/2018 12:26 Simple Gracias

    existe = this->iuc->ingresarCelular("090 76 54 32"); //Inicio sesion U2
    this->icc->seleccionarConversacionEnviar(2); //codigo de la conversacion privada recien creada
    this->icc->enviarMensajeSimple("Gracias");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U2

    //M8 CS3 U4 U3 23/05/2018 18:30 Simple Hola Pablo, ¿cómo estas?

    existe = this->iuc->ingresarCelular("090 66 67 77"); //Inicio sesion U4
    this->icc->seleccionarContacto("090 24 68 10"); //selecciono a U3
    this->icc->enviarMensajeSimple("Hola Pablo, ¿cómo estas?");
    this->iuc->cerrarGuasap(this->rlj->dimeLaHora());// cerro sesion U4
    */

}

void Menu::handleOption12() {

}

void Menu::handleExceptions(const string &eWhat) {

    /**
     * Toma un e.what() (donde 'e' es una excepcion del tipo std::invalid argument), y verificarel
     * si dicha excepcion fue lanzada por la funcion 'std::stoi()'. En caso positivo se muestra un
     * error generico de que se esperaba un valor numerico; sino, se muestra el e.what() recibido
     * por parametro.
     */

    if (eWhat == "stoi") {
        cout << endl << "   ---> Error: el dato ingresado debe ser numérico." << endl;
    }
    else {
        cout << endl << "   ---> Error: " << eWhat << endl;
    }
}

Menu::~Menu() {
    // Eliminamos las instancias  de Interfaz y Fabrica de UsuarioController.
    //delete this->iuc;
    delete this->fuc;

    // Eliminamos las instancias  de Interfaz y Fabrica de ConversacionController.
    //delete this->icc;
    delete this->fcc;

    // Eliminamos la instancia del reloj.
    delete this->rlj;

    // Dejamos la instancia del menu apuntando a null.
    instance = nullptr;
}
