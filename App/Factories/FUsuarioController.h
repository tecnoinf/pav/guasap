//
// Created by sebiitta on 10/06/18.
//

#ifndef GUASAP_FUSUARIOCONTROLLER_H
#define GUASAP_FUSUARIOCONTROLLER_H


#include "../Interfaces/IUsuarioController.h"

class FUsuarioController {
    private:
        // Instancia de la fabrica.
        static FUsuarioController* instance;

        // Constructor
        FUsuarioController();

    public:
        static FUsuarioController* getInstance();
        IUsuarioController* getIUsuarioController();

        // Destructor
        ~FUsuarioController();
};


#endif //GUASAP_FUSUARIOCONTROLLER_H
