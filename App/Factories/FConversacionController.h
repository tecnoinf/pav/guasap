//
// Created by sebiitta on 10/06/18.
//

#ifndef GUASAP_FCONVERSACIONCONTROLLER_H
#define GUASAP_FCONVERSACIONCONTROLLER_H


#include "../Interfaces/IConversacionController.h"

class FConversacionController {
    private:
        static FConversacionController* instancia;
        FConversacionController();

    public:
        static FConversacionController* getInstancia();
        IConversacionController* getIConversacionController();

        // Destructor
        ~FConversacionController();
};


#endif //GUASAP_FCONVERSACIONCONTROLLER_H
