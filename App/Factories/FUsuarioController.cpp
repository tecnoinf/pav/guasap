#include "../Controllers/UsuarioController.h"
#include "FUsuarioController.h"

// Instancia de la fabrica.
FUsuarioController* FUsuarioController::instance = nullptr;

// Constructor.
FUsuarioController::FUsuarioController() = default;

// Retornamos la instancia existente, o una nueva.
FUsuarioController* FUsuarioController::getInstance() {

    if (instance == nullptr) {
        instance = new FUsuarioController();
    }

    return instance;
}

IUsuarioController* FUsuarioController::getIUsuarioController() {
    return UsuarioController::getInstance();
}

// Dejamos el puntero a la instancia que se esta eliminando apuntando a null.
FUsuarioController::~FUsuarioController() {
    instance = nullptr;
}
