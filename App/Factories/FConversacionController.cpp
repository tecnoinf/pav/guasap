#include "../Controllers/ConversacionController.h"
#include "FConversacionController.h"

FConversacionController* FConversacionController::instancia = nullptr;

FConversacionController::FConversacionController() = default;

FConversacionController* FConversacionController::getInstancia() {
    if (instancia == nullptr) {
        instancia = new FConversacionController();
    }

    return instancia;
}

IConversacionController* FConversacionController::getIConversacionController() {

    return ConversacionController::getInstance();

}

FConversacionController::~FConversacionController() {
    instancia = nullptr;
}
