//
// Created by seba on 09/06/18.
//

#include "Fecha.h"

Fecha::Fecha()
{
    this->anio=1900;
    this->mes=01;
    this->dia=01;
}

Fecha::Fecha(int anio, int mes, int dia)
{
    this->anio=anio;
    this->mes=mes;
    this->dia=dia;

}

int Fecha::getAnio()
{
    return anio;
}

int Fecha::getMes()
{
    return mes;
}

int Fecha::getDia()
{
    return dia;
}
