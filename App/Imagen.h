#ifndef GUASAP_IMAGEN_H
#define GUASAP_IMAGEN_H

#include "Mensaje.h"
#include "Multimedia.h"
#include "Tipos.h"
#include "DtMensaje.h"
#include "Usuario.h"
#include <string>



class Imagen :public Multimedia{

    private:

        FormatoImg formato;
        int tamanio;
        string texto, url;

    public:

    //Constructores
    Imagen();
    Imagen(string codigo, FechaHora fhEnviado, FormatoImg formato, int tamanio, string texto, string url);

    //Getters
    FormatoImg getFormato();
    int getTamanio();
    string getTexto();
    string getUrl();

    //Setters
    void setFormatoImg(FormatoImg formato);
    void setTamanio(int tamanio);
    void setTexto(string texto);
    void setUrl(string url);

    //Operaciones
    DtMensaje& getDatos(string tel,Usuario* us);


    //Destructor
    ~Imagen();
};



#endif //GUASAP_IMAGEN_H
