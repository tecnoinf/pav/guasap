//
// Created by seba on 23/06/18.
//

#ifndef GUASAP_RELOJ_H
#define GUASAP_RELOJ_H


#include "FechaHora.h"

class Reloj {
private:
    // Instancia del Reloj
    static Reloj* instance;

    // Constructores
    Reloj();
    Reloj(bool usarHoraSistema, FechaHora hora);

    bool usarHoraSistema;
    FechaHora hora;

public:
    static Reloj* getInstance();

    // Operacion que retorna la hora del sistema operativo.
    FechaHora getHoraSistema();

    // dimeLaHora retorna el resultado de 'getHoraSistema()' o el valor de 'hora',
    // segun si se define utilizar la hora del sistema o no.
    FechaHora dimeLaHora();

    // Setters
    void setHora(FechaHora hora);

    // Getters
    void setUsarHoraSistema(bool HoraSis);
    bool getUsarHoraSistema();

    // Destructor
    ~Reloj();
};


#endif //GUASAP_RELOJ_H
