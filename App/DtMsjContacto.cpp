//
// Created by sebiitta on 09/06/18.
//

#include "DtMsjContacto.h"

DtMsjContacto::DtMsjContacto(): DtMensaje() {
    this->nombre = "";
    this->numTelefono = "";
}

DtMsjContacto::DtMsjContacto(string nombre, string numTelefono, string codigo, FechaHora fhEnviado): DtMensaje(codigo, fhEnviado) {
    this->nombre = nombre;
    this->numTelefono = numTelefono;
}

string DtMsjContacto::getNombre() {
    return this->nombre;
}

string DtMsjContacto::getNumTelefono() {
    return this->numTelefono;
}
