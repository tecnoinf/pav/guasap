#include "UsuarioConversacion.h"
#include "Usuario.h"
#include "Conversacion.h"
#include "DtConversacion.h"


//Contructores

UsuarioConversacion::UsuarioConversacion(){
	this->estaActiva = false;
	this->usuario = nullptr;
	this->conversacion = nullptr;
}

UsuarioConversacion::UsuarioConversacion(bool estaActiva, Usuario* usuario, Conversacion* conversacion){
	this->estaActiva = estaActiva;
	this->usuario = usuario;
	this->conversacion = conversacion;
}

//Getters

bool UsuarioConversacion::getEstaActiva(){
	return estaActiva;
}

Usuario* UsuarioConversacion::getUsuario(){
	return usuario;

}

Conversacion* UsuarioConversacion::getConversacion(){
	return conversacion;
}

//Setters

void UsuarioConversacion::setEstaActiva(bool activa){
	this->estaActiva = activa;
}

void UsuarioConversacion::setUsuario(Usuario* usuario){
	this->usuario = usuario;
}

void UsuarioConversacion::setConversacion(Conversacion* conversacion){
	this->conversacion = conversacion;
}

//Operaciones

void UsuarioConversacion::addConversacion(Conversacion* conv){
	setConversacion(conv);
}

void UsuarioConversacion::archivar(){
	setEstaActiva(false);
}

DtConversacion* UsuarioConversacion::getDatosConversacion(){
	Conversacion* conv = getConversacion();
	DtConversacion* dtConv = conv->getDatos();
	return dtConv;
}

//Destructor

UsuarioConversacion::~UsuarioConversacion(){

}

