
#include "DtConvPrivada.h"
#include "DtConversacion.h"
#include <string>

using std::string;

//Contructores
DtConvPrivada::DtConvPrivada() :DtConversacion(){
    this->numTelefono = "";
    this->nombre = "";
}

DtConvPrivada::DtConvPrivada(int id, string numTelefono, string nombre) :DtConversacion(id){
    this->numTelefono = numTelefono;
    this->nombre = nombre;
}

//Getters

string DtConvPrivada::getNumTelefono() {
    return numTelefono;
}

string DtConvPrivada::getNombre() {
    return nombre;
}
