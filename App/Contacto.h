//
// Created by sebiitta on 10/06/18.
//

#ifndef GUASAP_CONTACTO_H
#define GUASAP_CONTACTO_H

#include "DtMensaje.h"
#include "Usuario.h"
#include "DtContacto.h"
#include "DtMsjContacto.h"

class Contacto: public Mensaje {
    private:
        Usuario* usuarioAsociado;

    public:
        Contacto();
        Contacto(string codigo, FechaHora fhEnviado);

        void agregarContactoAMsj(Usuario* aenviar);
        DtMensaje& getDatos(string tel, Usuario* us);

        ~Contacto();
};


#endif //GUASAP_CONTACTO_H
