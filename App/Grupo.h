//
// Created by seba on 09/06/18.
//

#ifndef GUASAP_GRUPO_H
#define GUASAP_GRUPO_H

#include <string>
#include "FechaHora.h"
#include "UsuarioGrupo.h"
#include <map>

using namespace std;

class UsuarioGrupo;

class Grupo{

    private:
        string nombre, imagen;
        FechaHora fhCreacion;
        map <string, UsuarioGrupo*> colUsuarioGrupo;

    public:
        Grupo();
        Grupo(string nombre, string imagen, FechaHora fhCreacion);

        string getNombre();
        string getImagen();
        FechaHora getCreacion();
        UsuarioGrupo* getUsuarioGrupo(string clave);

        void setNombre(string nombre);
        void setImagen(string imagen);
        void setFhCreacion(FechaHora fhCreacion);
        void setUsuarioGrupo(string clave, UsuarioGrupo* usuarioGrupo);

        FechaHora getFhMiembro(string tel);

        ~Grupo();

};

#endif //GUASAP_GRUPO_H
