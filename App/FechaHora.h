//
// Created by seba on 09/06/18.
//

#ifndef GUASAP_DTFECHAHORA_H
#define GUASAP_DTFECHAHORA_H

#include "Fecha.h"

class FechaHora: public Fecha{
    private:
        int hora, minuto, segundo;

    public:

        FechaHora();
        FechaHora(int anio, int mes, int dia, int hora, int minuto, int segundo);

        int getHora();
        int getMinuto();
        int getSegundo();

        bool operator> (FechaHora f2);
        bool operator< (FechaHora f2);
        bool operator== (FechaHora f2);
};

#endif //GUASAP_DTFECHAHORA_H
