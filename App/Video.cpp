
#include "Video.h"
#include "Mensaje.h"
#include "DtMsjVideo.h"

//Constructores
Video::Video(){

    this->duracion = 0;
    this->url = "";
}

Video::Video(string codigo, FechaHora fhEnviado, int duracion, string url): Multimedia(codigo, fhEnviado){

    this->duracion = duracion;
    this->url = url;
}


//Getters
int Video::getDuracion(){

    return duracion;
}

string Video::getUrl(){

    return url;
}

DtMensaje& Video::getDatos(string tel, Usuario *us){

    ///hay que chequear que 'tel' existe para que no explote

    getUsuarioMensaje(tel)->setVisto(true);
    //aux->setVisto(true);
    auto dtmv = new DtMsjVideo(this->duracion, this->url, getCodigo(), getFhEnviado());
    return *dtmv;
}

//Setters
void Video::setDuracion(int duracion){

    this->duracion = duracion;
}

void Video::setUrl(string url){

    this->url = url;
}

//Destructor
Video::~Video(){


}
