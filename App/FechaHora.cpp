//
// Created by seba on 09/06/18.
//
#include "FechaHora.h"
#include "Fecha.h"


FechaHora::FechaHora():Fecha()
{
    this->hora=00;
    this->minuto=00;
    this->segundo=00;

}

FechaHora::FechaHora(int anio, int mes, int dia, int hora, int minuto, int segundo): Fecha(anio, mes, dia)
{
    this->hora=hora;
    this->minuto=minuto;
    this->segundo=segundo;
}

int FechaHora::getHora()
{
    return hora;
}

int FechaHora::getMinuto()
{
    return minuto;
}

int FechaHora::getSegundo()
{
    return segundo;
}

bool FechaHora::operator> (FechaHora f2) {
    return this->getAnio() > f2.getAnio() && this->getMes() > f2.getMes() && this->getDia() > f2.getDia() && this->hora > f2.hora && this->minuto > f2.minuto && this->segundo > f2.segundo;
}

bool FechaHora::operator< (FechaHora f2) {
    return this->getAnio() < f2.getAnio() && this->getMes() < f2.getMes() && this->getDia() < f2.getDia() && this->hora < f2.hora && this->minuto < f2.minuto && this->segundo < f2.segundo;
}

bool FechaHora::operator== (FechaHora f2) {
    return this->getAnio() == f2.getAnio() && this->getMes() == f2.getMes() && this->getDia() == f2.getDia() && this->hora == f2.hora && this->minuto == f2.minuto && this->segundo == f2.segundo;
}
